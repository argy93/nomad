---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Forms


<!-- START_dc49219aba26987a6f91e75ae9767057 -->
## Callback

> Example request:

```bash
curl -X POST "http://localhost/api/callback" \
    -H "Content-Type: application/json" \
    -d '{"name":"tempora","email":"omnis","telegram":"et"}'

```

```javascript
const url = new URL("http://localhost/api/callback");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "tempora",
    "email": "omnis",
    "telegram": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```
> Example response (422):

```json
null
```
> Example response (500):

```json
null
```

### HTTP Request
`POST api/callback`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | User name
    email | string |  required  | User email
    telegram | string |  optional  | User telegram

<!-- END_dc49219aba26987a6f91e75ae9767057 -->


