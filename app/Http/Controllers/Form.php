<?php


namespace App\Http\Controllers;


use App\Mail\CallbackClient;
use App\Mail\CallbackUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

/**
 * @group Forms
 * @package App\Http\Controllers
 */
class Form extends Controller {

    /**
     * Callback
     *
     * @param Request $request
     * @bodyParam name string required User name
     * @bodyParam email string required User email
     * @bodyParam telegram string User telegram
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @response 200 null
     * @response 422 null
     * @response 500 null
     */
    function callback(Request $request) {
        try {
            $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
//                'telegram' => 'filled'
            ]);
        } catch ( ValidationException $e ) {
            return response(null, $e->status);
        }

        Mail::to($request->email)->send(new CallbackUser($request->toArray()));
        Mail::send(new CallbackClient($request->toArray()));
    }

    function login(Request $request) {

    }

    function auth(Request $request) {

    }

    function test(Request $request) {

    }
}
