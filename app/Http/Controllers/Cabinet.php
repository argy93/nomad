<?php

namespace App\Http\Controllers;

use App\Mail\CallbackClient;
use App\Mail\CallbackUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Cabinet extends Controller
{
    public function showCabinetHomePage()
    {
        $test_passed = Auth::user()->test_passed;

        return view('cabinet.home', [
            'test_passed' => boolval($test_passed)
        ]);
    }

    public function showCabinetTestPage()
    {
        if (Auth::user()->test_passed === 1) return redirect()->route('cabinet.home');

        return view('cabinet.test');
    }

    public function showCabinetTestSuccessPage()
    {
        return view('cabinet.test-success');
    }

    public function showCabinetAchievePage()
    {
        return view('cabinet.achieve');
    }

    public function showCabinetToolsPage()
    {
        return view('cabinet.tools');
    }

    public function showCabinetToolsSuccessPage()
    {
        $user = Auth::user();
        Mail::to($user->email)->send(new CallbackUser(["name" => $user->name]));
        return view('cabinet.tools-success');
    }
}
