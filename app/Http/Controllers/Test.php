<?php

namespace App\Http\Controllers;

use App\Mail\TestToUser;
use App\User;
use Illuminate\Http\Request;
use App\Test as ModelTest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class Test extends Controller
{

    public function showIntroPage()
    {
        return view('test.intro');
    }

    public function showTestPage()
    {
        if (Auth::user()->test_passed === 1) return redirect()->route('cabinet.home');

        return view('test.test');
    }

    public function showGreatPage()
    {
        return view('test.great');
    }

    public function showLoginPage()
    {
        return view('login', ['redirect' => route('test.test')]);
    }

    public function showRegistrationPage()
    {
        return view('registration', ['redirect' => route('test.test')]);
    }

    public function passTest(Request $request)
    {
        $validation = $this->validator($request->all())->validate();
        $this->saveTest($request->all());
        $this->sendEmail($request->all());

        $redirect = route('test.submit');

        if ($request->input('redirect') && $request->input('redirect') !== '') {
            $redirect = $request->input('redirect');
        }

        return redirect($redirect);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_country'        => ['required', 'string', 'max:255'],
            'second_country'       => ['required', 'string', 'max:255'],
            'third_country'        => ['required', 'string', 'max:255'],
            'citizenship'          => ['required', 'string', 'max:255'],
            'current_income'       => ['required', 'string', 'max:255'],
            'annual_income'        => ['required', 'string', 'max:255'],
            'available_investment' => ['required', 'string', 'max:255'],
            'spouse'               => ['required', 'boolean'],
        ]);
    }

    protected function saveTest(array $data)
    {
        $test                       = new ModelTest();
        $test->user_id              = Auth::id();
        $test->like_to_live         = json_encode([
            'first_country'  => $data['first_country'],
            'second_country' => $data['second_country'],
            'third_country'  => $data['third_country'],
        ]);
        $test->citizenship          = $data['citizenship'];
        $test->current_income       = intval($data['current_income']);
        $test->annual_income        = intval($data['annual_income']);
        $test->available_investment = intval($data['available_investment']);
        $test->spouse               = $data['spouse'];

        $test->save();

        $user = User::where('id', '=', Auth::id())->firstOrFail();
        $user->test_passed = 1;
        $user->save();
    }

    protected function sendEmail(array $data) {
        $user = User::where('id', '=', Auth::id())->firstOrFail();

        Mail::to($user->email)->send(new TestToUser($data));
    }
}
