<?php

use Intervention\Image\ImageManagerStatic as Image;
use \Illuminate\Support\Facades\Lang;

/**
 * resize example [
 *  767 => 400 // 767 viewport | 400 image width
 * ]
 *
 *
 * @param string $asset_path - path to image, same as asset('path)
 * @param number $dimension - current image scale size 1 2 3 4
 * @param array $resize - array of points
 *
 * @return string
 */
function srcset($asset_path, $dimension, $resize = []) {
    $sizes = [4, 3, 2, 1.5, 1.25, 1];
    $path_to_file = public_path().$asset_path;
    $complex_path = "\n";

    foreach ($sizes as $size) {
        $new_path = str_replace("@{$dimension}x", "-w-auto@{$size}x", $path_to_file);
        $url = asset(str_replace("@{$dimension}x", "-w-auto@{$size}x", $asset_path));

        if ($size <= $dimension) {
            _resize_and_save($path_to_file, $new_path, $dimension, $size);
        }

        if (count($resize) > 0) {
            foreach ($resize as $viewport => $image_size) {
                $new_path = str_replace("@{$dimension}x", "-w{$viewport}@{$size}x", $path_to_file);
                $url = asset(str_replace("@{$dimension}x", "-w{$viewport}@{$size}x", $asset_path));
                _resize_and_save($path_to_file, $new_path, $dimension, $size);

                $complex_path .= "$url w$viewport {$size}x, \n";
            }
        }

        if ($size <= $dimension) {

            $complex_path .= "$url {$size}x, \n";
        }
    }

    return $complex_path;
}

function _resize_and_save($path, $new_path, $dimension, $size, $force_width = false) {
    if (file_exists($new_path)) return;

    $image = Image::make($path);
    $width = $image->getWidth();

    if ($force_width)  $width = $force_width;

    $new_size = $width/$dimension*$size;
    $image->resize($new_size, null, function ($constraint) {
        $constraint->aspectRatio();
    });
    $image->save($new_path);

    unset($image);
}

function get_countries_to_select() {
    $from_lang = Lang::get('countries.extended_list');
    $prepared = [];

    foreach ($from_lang as $value ) {
        $prepared[] = [
            'value' => $value['code'],
            'label' => $value['name']
        ];
    }

    return $prepared;
}

function get_users_counter() {
    $start = 1574399698;
    $now = time();
    $users = 18731;

    $diff = $now - $start;

    return $users + floor($diff / 491);
}
