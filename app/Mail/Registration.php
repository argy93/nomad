<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class Registration extends Mailable
{
    use Queueable, SerializesModels;
    protected $text;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->text["name"] = $data["name"];
        $this->text["link"] = route('cabinet.home');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view( 'email.registration', ["text" => $this->text ] )->subject('Nomad Registration');
    }
}
