<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class CallbackUser extends Mailable {
    use Queueable, SerializesModels;
    private $text;

    /**
     * Create a new message instance.
     *
     * @param array data
     *
     * @return void
     */
    public function __construct( $data ) {
        $this->text["name"] = $data["name"];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        ini_set('max_execution_time', 900);
        return $this->view( 'email.callback_user', ["text" => $this->text ] )
                    ->subject('Nomad 30 Tools')
                    ->attach(public_path().'/storage/common/List of Nomad Services for desktop.pdf')
                    ->attach(public_path().'/storage/common/List of Nomad Services for mobile.pdf');
    }
}
