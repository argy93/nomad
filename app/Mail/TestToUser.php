<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class TestToUser extends Mailable
{
    use Queueable, SerializesModels;
    private $params;
    private $text;
    private $country_prices = [
        'SA' => [
            'live' => 2700,
            'invest' => 4500,
        ],
        'AF' => [
            'live' => 2200,
            'invest' => 0,
        ],
        'AS' => [
            'live' => 5000,
            'invest' => 10000,
        ],
        'EU' => [
            'live' => 5400,
            'invest' => 55000,
        ],
        'NA' => [
            'live' => 4500,
            'invest' => 5000,
        ],
        'OC' => [
            'live' => 5800,
            'invest' => 50000,
        ],
    ];

    /**
     * Create a new message instance.
     *
     * @param  array  $data
     */
    public function __construct(array $data)
    {
        $this->params = $data;

        $text = [];
        $text["p1"] = $this->getCountriesAnswer($data['first_country'], $data['second_country'], $data['third_country'], intval($data['current_income']));
        $text["p2"] = $this->annualAnswer(intval($data['current_income']));
        $text["p3"] = "Reachable figure in less than a year. Our community is proof of this. Use our tips and very soon you will reach the required level.";
        $text["p4"] = $this->investAnswer(intval($data['available_investment']));
        $text["p5"] = $this->spouseAnswer($data['spouse']);
        $text["name"] = Auth::user()->name;

        $this->text = $text;
    }

    protected function getCountriesAnswer($first, $second, $third, $income) {
        $can_live_msg = 'These countries are absolutely right for you';
        $cannot_live_msg = 'You are not yet ready to live in these countries. Read the answers to the test further and you will find a solution';
        $can = true;

        if ($this->country_prices[$first]["live"] >= $income || $this->country_prices[$second]["live"] >= $income || $this->country_prices[$third]["live"] >= $income) {
            $can = false;
        }

        if ($can) return $can_live_msg;
        else return $cannot_live_msg;
    }

    protected function annualAnswer($income) {
        if ($income <= 2000) {
            return "Your current income is not high enough to live in any country in the world. It is worth paying attention to passive income and private investment in high-growth assets. Now these are artificial intelligence startups, e-commerce, cryptocurrencies and trading bots. We recommend developing your skills and selling them through the Upwork service, as well as using more reliable investments with monthly payouts from https://www.bitsonar.com/";
        } else if ($income >= 2001 && $income <= 5999) {
            return "Congratulations, you are most likely either a good specialist, or are able to sell your skills in order to feel at an average level in any country in the world. But it is on average. To feel completely free with your income  you should look at short high-yield investments that provide from 50 - 120% per annum. And in parallel, develop your skills and monetize them on Upwork. If you still want to quickly get income not from the work done, but from investing in reliable technologies, then use the services of the Bitsonar fund https://www.bitsonar.com/";
        } else if ($income > 6000) {
            return "Your current income allows you to move to any country in the world. But to be a real Nomad and to lead the desired lifestyle, you should have a constant passive income, even when you just relax, ride a surf, travel to exotic countries. We recommend using medium-term investments in reliable funds, which are used by more than a million of the same ambitious specialists like you and who bring them monthly income. The most highly profitable assets now are cryptocurrencies, but at the same time risky. To optimize the diversification of investments and risks in order to double your capital in less than a year, take a look at Sequoia Capital and Pantera funds, but be prepared for an investment of more than 250 thousand. If you want an even more reliable service, where all the experts have already done everything in one product, and you do not need to understand cryptocurrencies, then this is the Bitsonar fund https://www.bitsonar.com/, that allows you to earn from 50 - 120%.";
        }

        return "";
    }

    protected function investAnswer($invest) {
        if ($invest <= 5000) {
            return "Already now you can afford to get a residence in Paraguay, Ecuador, Chile, Panama or Argentina";
        } else if ($invest > 5000 && $invest < 50000) {
            return "Consider an additional residence in Andorra, Estonia, the Philippines.";
        } else if ($invest > 50000) {
            return "You can afford residence in more than 20 countries of the world.";
        }

        return "";
    }

    protected function spouseAnswer($spouse) {
        if (boolval($spouse)) {
            return "But, you need to earn at least twice as much. And now you are already taking the basic steps. keep it up";
        }

        return "";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.test_to_user', ["text" => $this->text])
            ->subject('Nomad Test');
    }
}
