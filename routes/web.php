<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home@showHomePage')->name('home');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@wrappedLogin');

Route::get('/registration', 'Auth\RegisterController@showRegistrationForm')->name('registration');
Route::post('/registration', 'Auth\RegisterController@controlledRegistration');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'test', 'as' => 'test.'], function () {
    Route::get('/', 'Test@showIntroPage')->name('home');
    Route::get('/form', 'Test@showTestPage')->middleware('testauth')->name('test');
    Route::get('/login', 'Test@showLoginPage')->name('login');
    Route::get('/registration', 'Test@showRegistrationPage')->name('registration');

    Route::post('/test-submit', 'Test@passTest')->middleware('auth')->name('submit');
    Route::get('/test-submit', 'Test@showGreatPage')->middleware('auth')->name('submit');
});

Route::group(['middleware' => 'auth', 'prefix' => 'cabinet', 'as' => 'cabinet.'], function () {
    Route::get('/', 'Cabinet@showCabinetHomePage')->name('home');
    Route::get('/test/success', 'Cabinet@showCabinetTestSuccessPage')->name('test-success');
    Route::get('/test', 'Cabinet@showCabinetTestPage')->name('test');
    Route::get('/achieve', 'Cabinet@showCabinetAchievePage')->name('achieve');
    Route::get('/tools/success', 'Cabinet@showCabinetToolsSuccessPage')->name('tools-success');
    Route::get('/tools', 'Cabinet@showCabinetToolsPage')->name('tools');
});
