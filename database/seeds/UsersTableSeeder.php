<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return User::create([
            'name'        => 'Konstantine Klo',
            'email'       => 'k@gmail.com',
            'password'    => bcrypt('password'),
            'test_passed' => false,
        ]);
    }
}
