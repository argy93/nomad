import hljs from 'highlight.js';

hljs.configure({
  languages: ['html', 'php', 'highlightjs-alan']
});
hljs.initHighlightingOnLoad();
