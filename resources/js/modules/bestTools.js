import $ from 'jquery';
import UIkit from 'uikit';

export default () => {
  const $bestTools = $('.js-best-tools');

  if (!$bestTools.length) return;

  const $form = $bestTools.find('.js-best-tools-form');
  const $cover = $bestTools.find('.js-best-tools-cover');
  const formWidth = $form.outerWidth();
  const formHeight = $form.outerHeight();

  $bestTools.css({
    width: formWidth,
    height: formHeight,
  });

  $cover.css({
    width: formWidth,
    height: formHeight,
  });

  UIkit.scrollspy($cover[0]);

  $cover.on('inview', () => {
    setTimeout(() => {
      $bestTools.addClass('is-visible');

      if (window.innerWidth >= 1025) {
        $bestTools.css({
          width: '100%'
        });
      }
    }, 300)
  });
}
