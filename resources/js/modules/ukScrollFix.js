import $ from 'jquery';
import UIkit from 'uikit';

export default () => {
  $('[uk-scroll]').click(function() {
    UIkit.offcanvas($('#mobile-menu')[0]).hide();
  });

  $('[uk-scroll]').each(function() {
    const $item = $(this);

    if (!$('#'+$item.attr('href').split('#').pop()).length) {
      $item.removeAttr('uk-scroll');
    }
  });
}
