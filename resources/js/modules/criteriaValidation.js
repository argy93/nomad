import $ from 'jquery';

export default () => {
  const criteriaPoll = $('.js-criteria-poll');
  let checkCounter = 0;

  criteriaPoll.on('change', function () {
    checkCounter = criteriaPoll.find('input[type=checkbox]:checked').length;
    buttonDisableHandler(checkCounter);
  });

  const buttonDisableHandler = () => {
    if (checkCounter >= 4) {
      criteriaPoll.find('.js-criteria-btn').removeClass('is-disabled');
    } else {
      criteriaPoll.find('.js-criteria-btn').addClass('is-disabled');
    }
  };

  criteriaPoll.find('.js-criteria-btn').click((event) => {
    if (checkCounter < 4) {
      event.preventDefault();
    }
  });

  buttonDisableHandler();
}


