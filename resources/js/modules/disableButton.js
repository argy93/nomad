import $ from 'jquery';

export default () => {
  $('.c-button').click(function(event) {
    if ( $(this).hasClass('is-disabled') ) event.preventDefault();
  })
}
