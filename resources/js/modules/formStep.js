import $ from 'jquery';

export default () => {
  const prefix = '.js-fs';
  const tabStepClass = `${prefix}-tab-step`;
  const tabBodyClass = `${prefix}-tab-body`;
  const buttonClass = `${prefix}-button`;
  const nextButtonClass = `${prefix}-button-next`;
  const activeClass = 'is-active';
  const $tabContainer = $(`${prefix}-container`);

  $tabContainer.find(buttonClass).click(function() {

    if ($(this).prop('disabled') || $(this).hasClass('is-disabled') || !isStepValid($tabContainer.closest(tabBodyClass))) return;

    const target = $(this).attr('data-tab');

    $tabContainer.find(tabBodyClass).removeClass(activeClass);
    $tabContainer.find(target).addClass(activeClass);

    $tabContainer.find(tabStepClass).removeClass(activeClass);
    $tabContainer.find(`${tabStepClass}[data-id="${target}"]`).prevAll().addClass(activeClass);

  });

  const $tabBody = $tabContainer.find(tabBodyClass);

  $tabBody.each(function() {
      const $step = $(this);

      $step.find('select, input, radio').on('change', function() {
        if (isStepValid($step)) {
          enableNextButton($step);
        } else {
          disableNextButton($step);
        }
      });
  });

  function isStepValid($step) {
    let valid = true;

    $step.find('select, input').each(function() {
      const val = $(this).val();

      if (!val || val === '') {
        valid = false;
      }
    });

    const $spouse = $step.find('radio[name="spouse"]');

    if ($spouse.length) {
      const val = $spouse.filter(':checked').val();

      if (!val || val === '') {
        valid = false;
      }
    }

    return valid;
  }

  function enableNextButton($step) {
    $step.find(nextButtonClass).prop('disabled', false).removeClass('is-disabled');
  }

  function disableNextButton($step) {
    $step.find(nextButtonClass).prop('disabled', true).addClass('is-disabled');
  }

}
