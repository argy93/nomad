import $ from 'jquery';

export default () => {
  const customInput = $('.js-movable-placeholder').find('input');

  customInput.focus(function () {
    const $target = $(this);

    if ( emptyTargetHandler($target) ) {
      $target.parent().addClass('is-focused');
    }
  });

  customInput.focusout(function () {
    const $target = $(this);

    if ( emptyTargetHandler($target) ) {
      $target.val('');
      $target.parent().removeClass('is-focused');
    }
  });

  const emptyTargetHandler = ($target) => {
    return $target.val() === '' || $target.val().trim() === ''
  }
}
