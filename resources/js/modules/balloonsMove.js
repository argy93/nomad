import $ from 'jquery';

export default () => {
  /**
   * @typedef {Object} Point
   * @property {number} x - The X Coordinate
   * @property {number} y - The Y Coordinate
   */

  $(window).on('mousemove', balloonsHandler);

  /**
   * Calculate next balloon position
   * @param {Point} mousePosition
   * @param {number} gapX
   * @param {number} gapY
   * @returns {Point}
   */
  function getBalloonOffset(mousePosition, gapX, gapY) {
    const centerPosition = {
      x: mousePosition.x - (window.innerWidth / 2),
      y: mousePosition.y - (window.innerHeight / 2)
    }
    const percentPoint = convertPointToPercent(centerPosition);

    return {
      y: gapY / 100 * percentPoint.y,
      x: gapX / 100 * percentPoint.x
    };
  }

  /**
   * Convert point x and y to percent equals
   * @param {Point} point
   * @return {Point}
   */
  function convertPointToPercent(point) {

    return {
      x: point.x / (window.innerWidth / 100),
      y: point.y / (window.innerHeight / 100)
    }
  }

  function balloonsHandler(event) {

    $('.js-ballon').each(function (index, el) {
      const $el = $(el);
      const gapY = $el.data('gap-y');
      const gapX = $el.data('gap-x');

      const position = getBalloonOffset({
        x: event.clientX,
        y: event.clientY,
      }, gapX, gapY);

      $el.css({
        'transform': `translate(${position.x}px, ${position.y}px)`,
      });

    });
  }
}
