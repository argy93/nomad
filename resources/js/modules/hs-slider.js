import $ from 'jquery';
import 'malihu-custom-scrollbar-plugin';

export default () => {

  // console.log($('.hs-slider__row, .js-hs-slider'));
  if (!$('.hs-slider__row, .js-hs-slider').length) {
    return;
  }

  if ($(window).innerWidth() >= 768) {
    $(".hs-slider__row").mCustomScrollbar({
      axis:"x",
      theme:"light-3",
      advanced:{
        autoExpandHorizontalScroll:false,
        updateOnContentResize:false,
        updateOnImageLoad:true
      }
    });
  } else {

    $('.js-hs-slider').slick({
      autoplay: false,
      autoplaySpeed: 5000,
      speed: 1000,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      appendDots: $('.c-features-centered__dots'),
      touchMove: true,
      pauseOnFocus: false,
      pauseOnHover: false
    });

    if (true) {}

    function videoHeight() {
      var heightSliderImg = $('.c-meeting .is-mobile .hs-slider li img').height();
      console.log(heightSliderImg);

      $('.c-meeting .is-mobile .hs-slider').find('video').css('height', heightSliderImg + 'px');
    };

    videoHeight();

    $(window).resize(function() {
      videoHeight();
    }).trigger('resize');

  }

  $(".hs-slider__row").mCustomScrollbar("update");
}
