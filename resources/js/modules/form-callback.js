import $ from 'jquery';

export default () => {
  const $form = $('.js-form-callback');

  $form.validate({
    rules: {
      // no quoting necessary
      name: "required",
      // quoting necessary!
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      // dots need quoting, too!
      telegram: {
        rangelength: [2, 100]
      }
    },
    submitHandler: function(form) {
      const submittedForm = $(form);
      submittedForm.addClass('is-loading');

      $.ajax('/api/callback', {
        method: 'POST',
        data: submittedForm.serialize(),
        success: () => {
          submittedForm.removeClass('is-loading');
          submittedForm.trigger("reset");
          submittedForm.find('input').trigger('focusout');
          alert('Email was sent successfully');
        },
        error: () => {
          alert('Server error, try again later');
          submittedForm.removeClass('is-loading');
        }
      })
    }
  });

}
