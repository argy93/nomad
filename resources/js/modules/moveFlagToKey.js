import $ from 'jquery';
import {SVG} from '@svgdotjs/svg.js';

export default () => {
  const $flagSvg = window.innerWidth <= 559 ? $('.js-flag-svg-mobile') : $('.js-flag-svg-desktop');

  if (!$flagSvg.length) return;

  const $flag = $flagSvg.find('.js-flag');
  const flagSvg = SVG($flag[0]);
  const flagShadowSvg = SVG($flagSvg.find('.js-flag-shadow')[0]);
  const flagLine = SVG($flagSvg.find('.js-flag-line')[0]);
  const flagArena = SVG($flagSvg.find('.js-flag-arena')[0]);
  const point = $flagSvg[0].createSVGPoint();

  const $mainContainer = $('.c-archive-flag');
  const $flagSvgContainer = window.innerWidth <= 559 ? $('.c-archive-flag__svg-mobile') : $('.c-archive-flag__svg-desktop');

  const $flagSvgActive = window.innerWidth <= 559 ? $('.js-flag-svg-mobile-active') : $('.js-flag-svg-desktop-active');

  let mc;
  let flagGrabbing = false;
  let flagOnArena = false;

  if (!$flagSvg.length) return;

  init();

  function init() {
    setEvents();
  }

  function setEvents() {
    $flagSvg.on('mousemove', onMouseMove);
    $flagSvg.on('touchmove', onMouseMove);
    $flag.on('mousedown', onMouseDownOnFlag);
    $flag.on('touchstart', onMouseDownOnFlag);
    $flagSvg.on('mouseup', onMouseUpOnFlag);
    $flagSvg.on('touchend', onMouseUpOnFlag);
  }

  function onMouseDownOnFlag() {
    $flag.addClass('is-grab');
    flagGrabbing = true;
  }

  function onMouseUpOnFlag() {
    $flag.removeClass('is-grab');
    flagGrabbing = false;
  }

  function onMouseMove(e) {
    if (flagOnArena || !flagGrabbing) return;

    const point = cursorPoint(e);

    const transform = {
      translateX: (point.x - flagSvg.x()) - flagSvg.width() / 2,
      translateY: (point.y - flagSvg.y()) - flagSvg.height() / 2,
    };
    flagSvg.transform(transform);
    flagShadowSvg.transform(transform);

    window.innerWidth <= 559 ? flagLine.attr('d', `M160 240L${point.x} ${point.y}`) : flagLine.attr('d', `M647 375.5L${point.x} ${point.y}`);

    if (flagArena.inside(point.x, point.y)) {
      moveFlagToArena();
      $flagSvg.fadeOut('slow', function() {
        $flagSvgActive.fadeIn().addClass('is-icon-move');
      });

      $mainContainer.addClass('is-open');

      setTimeout(() => {
        $flagSvgContainer.addClass('is-open');
        window.location.href = $("#redirect").attr('data-content');
      }, 1000);


    }
  }

  function moveFlagToArena() {
    flagOnArena = true;

    flagLine.attr('d', '');

    const to = window.innerWidth <= 559 ? {
      translateX: -0.5,
      translateY: 153,
    } : {
      translateX: 310,
      translateY: 1,
    };

    flagSvg.transform(to);
    flagShadowSvg.transform(to);
    $flagSvg.addClass('is-active');
  }

  function cursorPoint(e) {
    if (!e.clientX) {
      point.x = e.originalEvent.targetTouches[0].clientX;
      point.y = e.originalEvent.targetTouches[0].clientY;
    } else {
      point.x = e.clientX;
      point.y = e.clientY;
    }

    return point.matrixTransform($flagSvg[0].getScreenCTM().inverse());
  }
}
