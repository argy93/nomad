import $ from 'jquery';
import 'select2/dist/js/select2.full';


export default () => {
  const options = {
    placeholder: 'Chose country...',
    containerCssClass: 'c-dropdown'
  };
  const $dropdown = $('.js-dropdown');

  $dropdown.each(function (i, item) {
    const copy = Object.assign({}, options);

    if (!$(item).hasClass('is-extended')) {
      copy.minimumResultsForSearch = Infinity;
    }

    $(item).select2(copy);
  });
}
