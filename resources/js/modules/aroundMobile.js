import $ from 'jquery';
import 'slick-carousel';

export default () => {
  let windowWidth = $(window).width();

  if (windowWidth < 560 ) {
    $('.c-around .js-slider').slick({
      dots: false,
      arrows: false,
      infinite: false,
      speed: 500,
      autoplay: false,
      autoplaySpeed: 5000,
      slidesToShow: 1.5,
      // centerMode: true,
      centerPadding: '50px',
      slidesToScroll: 1
    });
  }
}


