import $ from 'jquery';
import 'slick-carousel';

export default () => {
  if ($(window).innerWidth() <= 767) {

    $('.js-flags-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      infinite: true,
      fade: false,
      asNavFor: '.js-flags-icons-slider',
    });

    $('.js-flags-icons-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '.js-flags-slider',
      arrows: false,
      dots: false,
      infinite: true,
      variableWidth: true,
      centerMode: false,
      focusOnSelect: true
    });
  }
}
