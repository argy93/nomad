import $ from 'jquery';

export default () => {

  $('.js-range').each(init);

  function init() {
    const $input = $(this);
    update($input);

    $input.on('input', function() {
      update($input);
    })
  }

  function update($input) {
    const val = $input.val();
    $input.closest('.js-range-wrap').find('.js-range-value').text(val);
    const maxValue = $input.attr('max');
    // console.log(maxValue);
    if (val > 100) {
      const maxValuePercent = (val * 100)/maxValue;
      $input.css({'background':'-webkit-linear-gradient(left, #fff 0%, #fff '+maxValuePercent+'%, #C98F24 '+maxValuePercent+'%, #C98F24 100%)'});
    } else {
      $input.css({'background':'-webkit-linear-gradient(left, #fff 0%, #fff '+val+'%, #C98F24 '+val+'%, #C98F24 100%)'});
    }
  }
}
