import $ from 'jquery';
import shuffle from 'lodash.shuffle';

export default () => {
  if (!$('#member-template').length) return;

  let counters = {
    membersCount: 0,
    pointCount: 0
  };
  const template = $('#member-template').html();

  let membersName = JSON.parse($("#names-list").html());
  $("#names-list").remove();

  let pointsList = [
    {top: 14, left: 5},
    {top: 10, left: 10},
    {top: 5, left: 11},
    {top: 2, left: 19},
    {top: 13, left: 19},
    {top: 15, left: 14},
    {top: 20, left: 9},
    {top: 22, left: 14},
    {top: 21, left: 16},
    {top: 21, left: 24},
    {top: 24, left: 26},
    {top: 30, left: 23},
    {top: 30, left: 18},
    {top: 29, left: 12},
    {top: 35, left: 13},
    {top: 34, left: 16},
    {top: 32, left: 21},
    {top: 39, left: 21},
    {top: 42, left: 15},
    {top: 0, left: 27},
    {top: 3, left: 30},
    {top: 12, left: 31},
    {top: 54, left: 25},
    {top: 56, left: 27},
    {top: 61, left: 30},
    {top: 63, left: 32},
    {top: 68, left: 30},
    {top: 73, left: 29},
    {top: 77, left: 27},
    {top: 82, left: 25},
    {top: 66, left: 26},
    {top: 24, left: 47},
    {top: 27, left: 44},
    {top: 31, left: 44},
    {top: 31, left: 47},
    {top: 30, left: 49},
    {top: 33, left: 51},
    {top: 35, left: 53},
    {top: 27, left: 53},
    {top: 27, left: 49},
    {top: 22, left: 54},
    {top: 18, left: 51},
    {top: 17, left: 47},
    {top: 24, left: 58},
    {top: 18, left: 60},
    {top: 42, left: 56},
    {top: 42, left: 64},
    {top: 47, left: 65},
    {top: 42, left: 69},
    {top: 46, left: 71},
    {top: 41, left: 75},
    {top: 58, left: 75},
    {top: 75, left: 77},
    {top: 76, left: 82},
    {top: 78, left: 84},
    {top: 72, left: 84},
    {top: 83, left: 91},
    {top: 33, left: 62},
    {top: 34, left: 66},
    {top: 31, left: 78},
    {top: 45, left: 54},
    {top: 25, left: 68},
    {top: 1, left: 48},
    {top: 44, left: 41},
  ];

  const membersRender = () => {
    const nextTimeout = randomNumber(2, 5) * 1000;
    const nextMember = getNextMember();
    const nextPosition = getNextPosition();
    const userTemplate = getUserTemplate(nextMember, nextPosition);
    const duplicateUserTemplate = userTemplate.clone().addClass('is-bottom');

    setTimeout(() => {
      $('.js-map')
        .append(userTemplate)
        .append(duplicateUserTemplate);

      animate(userTemplate, membersRender);
      animate(duplicateUserTemplate);
    }, nextTimeout );
  };

  const animate = (item, cb = function () {}) => {
    item.fadeIn(500, function () {
      setTimeout(() => {
        item.fadeOut(500, function () {
          cb();
          item.remove();
        });
      }, 10000);
    });
  };

  const getNextMember = () => {
    return getNextElementFromArray( 'membersCount', membersName );
  };

  const getNextPosition = () => {
    return getNextElementFromArray( 'pointCount', pointsList );
  };

  const getNextElementFromArray = (counterName, arr) => {
    if ( !arr[counters[counterName]] ) {
      counters[counterName] = 0;
    }

    if ( counters[counterName] === 0 ) arr = shuffle(arr);

    return arr[counters[counterName]++];
  };

  const getUserTemplate = (name, point) => {
    let newUser = $(template);
    newUser
      .css({
        'top': `${point.top}%`,
        'left': `${point.left}%`,
      })
      .find('b')
      .html(name);

    return newUser;
  };

  const randomNumber = (min, max) => {
    let rand = min - 0.5 + Math.random() * (max - min + 1);

    return rand.toFixed(1);
  };

  membersRender();
}
