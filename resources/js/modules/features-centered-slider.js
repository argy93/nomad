import $ from 'jquery';

export default () => {
  if ($(window).innerWidth() <= 767) {
    $('.js-features-centered-slider').slick({
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1000,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      appendDots: $('.c-features-centered__dots'),
      touchMove: false,
      pauseOnFocus: false,
      pauseOnHover: false
    });
  }
}
