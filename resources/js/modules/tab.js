import $ from 'jquery';

export default () => {
  $('.js-tab').find('[data-target]').click(function() {
    const $tab = $(this);

    if ($tab.hasClass('is-active')) return;

    const $root = $tab.closest('.js-tab');
    const $activeTab = $root.find('[data-tab].is-active');
    const $activeButton = $root.find('[data-target].is-active');

    $activeTab.fadeOut('fast', () => {
      const $newTab = $root.find(`[data-tab="${$tab.data('target')}"]`);

      $newTab.fadeIn('medium', () => {
        $activeButton.removeClass('is-active');
        $activeTab.removeClass('is-active');
        $tab.addClass('is-active');
        $newTab.addClass('is-active');
        if ($newTab.data('type')) {
          $newTab.css('display', $newTab.data('type'));
        }
      });
    });
  });
}
