import $ from 'jquery';
import UIkit from 'uikit';

export default () => {
  const $flagAnimation = $('.js-flag-animation');

  if (!$flagAnimation.length) {
    return;
  }

  $flagAnimation.each((index, element) => {
    UIkit.scrollspy(element);

    $(element).on('inview', () => {
      anim(element);
    });
  });

  function anim(element) {
    const $element = $(element);
    const $icons = $element.find('.js-icons');

    $icons.each((i) => {
      setTimeout(() => {
        const findClass = `[data-id="${i+1}"]`;
        $element.find(findClass).css('opacity', '1');
      }, (2500 * (i + 1)) - (i * i * 100));
    });
  }
}
