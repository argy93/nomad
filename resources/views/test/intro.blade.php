@extends('layouts.full-size')

@section('title', 'About')
@section('page', 'intro')
@section('content')
  @include('components.test-intro')
@endsection
@section('travel')
  @include('components.travel')
@endsection
@section('footer')
  @include('components.footer')
@endsection
