<?php if (!isset($name)) $name = 'state'; ?>

<select class="
  js-dropdown
  @if ( isset($mod) && $mod === 'extended' )
    {{ ' is-' . $mod }}
  @endif
" name="{{ $name }}">
  <option disabled>Chose country...</option>
  <option></option>
  @foreach ( $options as $country )
    <option value="{{$country["value"]}}">{{$country["label"]}}</option>
  @endforeach
</select>
