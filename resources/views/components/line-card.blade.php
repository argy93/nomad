<?php if(!isset($align)) $align = 'left' ?>
<div class="c-line-card is-{{ $align }}">
  <div class="c-line-card__line"></div>
  <div class="c-line-card__text">
    {{ $slot }}
  </div>
</div>
