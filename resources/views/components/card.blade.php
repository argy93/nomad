<div class="c-card">
  <img src="{{ asset( $img ) }}" alt="">
  @if (isset($name))
    <span class="c-card__name c-text is-big is-white">{{ $name }}</span>
  @endif
  <p class="c-text is-white">{!! $text !!}</p>
</div>
