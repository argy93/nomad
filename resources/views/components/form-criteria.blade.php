<div class="c-form-criteria">
  @include('components.checkbox', ['id' => 'field-1', 'label' => 'you would like to live in more than 3 countries'])
  @include('components.checkbox', ['id' => 'field-2', 'label' => 'you are not afraid of frequent flights'])
  @include('components.checkbox', ['id' => 'field-3', 'label' => 'you can read and understand more than 3 pages of text (and at least read the information on this website)'])
  @include('components.checkbox', ['id' => 'field-4', 'label' => 'you are interested in new technologies'])
  @include('components.checkbox', ['id' => 'field-5', 'label' => 'you are interested or you are not afraid of studying personal finances'])
  @include('components.checkbox', ['id' => 'field-6', 'label' => 'you had business or investment experience'])
  @include('components.checkbox', ['id' => 'field-7', 'label' => 'you have no criminal record'])
</div>
