<div class="c-best-tools">
  <div class="u-container">
    <div
      class="c-title is-gold"
      uk-scrollspy="cls:uk-animation-fade; delay: 500"
    >
      <b>Get 30 ever best tools<br>to live anywhere</b>
    </div>
    <div class="c-best-tools__body js-best-tools">
      <div
        class="c-best-tools__cover js-best-tools-cover"
      >
        <img src="{{ asset('/storage/common/callback-form@1x.jpg') }}" srcset="{{ srcset('/storage/common/callback-form@4x.jpg', 4) }}" alt="">
      </div>
      <div
        class="c-best-tools__form js-best-tools-form"
      >
        @include('components.form-callback')
      </div>
    </div>
  </div>
</div>
