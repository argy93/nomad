<div class="c-ballons">
  <div class="c-ballons__animated">
    <img src="{{ asset('/storage/common/ballon1-min.png') }}" data-gap-y="100" data-gap-x="100" class="js-ballon c-ballons__first">
    <img src="{{ asset('/storage/common/ballon2-min.png') }}" data-gap-y="50" data-gap-x="50" class="js-ballon c-ballons__second">
    <img src="{{ asset('/storage/common/ballon3-min.png') }}" data-gap-y="20" data-gap-x="20" class="js-ballon c-ballons__third">
    <img src="{{ asset('/storage/common/ballon4-min.png') }}" data-gap-y="10" data-gap-x="10" class="js-ballon c-ballons__four">
    <span data-gap-y="60" data-gap-x="60" class="js-ballon c-ballons__cirlce v1"></span>
    <span data-gap-y="40" data-gap-x="40" class="js-ballon c-ballons__cirlce v2"></span>
    <span data-gap-y="20" data-gap-x="20" class="js-ballon c-ballons__cirlce v3"></span>
    <span data-gap-y="10" data-gap-x="10" class="js-ballon c-ballons__cirlce v4"></span>
    <span data-gap-y="5" data-gap-x="5" class="js-ballon c-ballons__cirlce v5"></span>
  </div>
  <div class="c-ballons__static">
    <img src="{{ asset('/storage/common/airbaloons.png') }}" alt="">
  </div>
</div>
