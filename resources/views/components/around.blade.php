<section class="c-around">
  <h2
    class="c-title is-gold"
    uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
  >
    <b>Nomad Family members are thousands of people around the world who chose:</b>
  </h2>
  <div class="c-around__container">
    <div
      class="c-around__flex js-slider"

    >
      <div class="c-around__col">
        @include('components.card', [
            'name' => 'The choise',
            'text' => 'To live in a country where you want',
            'img' => '/storage/common/choice-icon.svg'
          ])
      </div>
      <div class="c-around__col">
        @include('components.card', [
            'name' => 'Infinity',
            'text' => 'Travel and discover new places',
            'img' => '/storage/common/infinity-icon.svg'
          ])
      </div>
      <div class="c-around__col">
        @include('components.card', [
            'name' => 'Freedom',
            'text' => 'Have dual residence and the possibility of visa-free visits to most countries',
            'img' => '/storage/common/freedom-icon.svg'
          ])
      </div>
      <div class="c-around__col">
        @include('components.card', [
            'name' => 'Flexibility',
            'text' => 'Keep assets in a safe place',
            'img' => '/storage/common/flexibility-icon.svg'
          ])
      </div>
      <div class="c-around__col">
        @include('components.card', [
            'name' => 'Benefit',
            'text' => 'Reduce taxation for business. and even if it wasn’t before the six-month Nomad Family program, it appears in any form',
            'img' => '/storage/common/benefit-icon.svg'
          ])
      </div>
    </div>
  </div>
</section>
