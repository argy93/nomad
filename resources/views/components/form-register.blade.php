<?php if (!isset($redirect)) $redirect = route('cabinet.home')  ?>

<form action="{{ route('registration') }}" method="post" class="c-form-login">
  @csrf
  <input type="hidden" name="redirect" value="{{ $redirect  }}">

  <div class="c-form-login__content">
    <div class="c-form-login__header">
      <p class="c-title is-white is-small"><b>Nomad Family account registration</b></p>
      <p class="c-text is-white is-small uk-margin-top">Please enter your personal information to join The Family.</p>
    </div>
    <div class="c-form-login__body">

      @include('components.input', ['label' => 'Name', 'name' => 'name', 'style' => 'white', 'error' => $errors->first('name')])
      @include('components.input', ['label' => 'E-mail', 'type' => 'email', 'name' => 'email', 'style' => 'white', 'error' => $errors->first('email')])
      @include('components.input', ['label' => 'Password', 'type' => 'password', 'name' => 'password', 'style' => 'white', 'error' => $errors->first('password')])

      @if (count($errors))
        @include('components.error', [
          'content' => "<span class='c-text is-small is-white'>{$errors->first('name')} {$errors->first('email')} {$errors->first('password')}</span>"
        ])
      @endif

      <div class="c-form-login__buttons">
      @include('components.button', [
        'style' => 'white',
        'type' => 'button',
        'buttonType' => 'submit',
        'arrowRight' => $redirect !== route('cabinet.home'),
        'text' => $redirect == route('cabinet.home') ? 'create account' : 'start the test'
      ])
      </div>
    </div>
  </div>
  <div class="c-form-login__logo">
    @include('components.logo')
  </div>
</form>
