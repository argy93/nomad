<div class="hs-slider">
  <div class="hs-slider__row">
    <ul class="js-hs-slider">
      @if (isset($videos))
        @foreach ($videos as $src)
          <li class="hs-slider__video">
              <video src="{{ asset($src) }}" controls poster="{{ asset('/storage/common/video-poster.jpg') }}"></video>
          </li>
        @endforeach

      @endif
      @foreach ($images as $item)
        <li><img src="{{ $item }}"></li>
      @endforeach
    </ul>
  </div>
</div>
