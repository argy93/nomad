<div class="c-members-counter">
  <span class="c-title is-white">{{ number_format(get_users_counter(), 0, '', ',') }}</span>
  <span class="c-text is-gold">Members</span>
</div>
