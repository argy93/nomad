<?php if ( ! isset($cabinet))
    $cabinet = false; ?>
<header class="c-main-header">
  @include('components.logo', ['big' => true])
  @include('components.logo')
  @if ($cabinet)
    <ul class="c-main-header__cabinet-menu">
      <li>
        <a href="{{ route('cabinet.test') }}">
          <div class="c-main-header__cabinet-menu-img">
            <svg width="35" height="27" viewBox="0 0 35 27" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.25 0.75H13.75V11.25H3.25V0.75ZM6.75 4.25V7.75H10.25V4.25H6.75ZM17.25 4.25H34.75V7.75H17.25V4.25ZM17.25 18.25H34.75V21.75H17.25V18.25ZM6.75 27L0.625 20.875L3.0925 18.4075L6.75 22.0475L14.7825 14.0325L17.25 16.5L6.75 27Z" fill="#CA9023"/>
            </svg>
          </div>
          <b>Test</b>
        </a>
      </li>
      <li>
        <a href="{{ route('cabinet.achieve') }}">
          <div class="c-main-header__cabinet-menu-img">
            <svg width="30" height="40" viewBox="0 0 30 40" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M29.7999 19.8C29.7977 17.0085 29.0263 14.2731 27.5726 11.9013C26.1188 9.5296 24.0404 7.61574 21.5711 6.37508V9.80007H18.2795V5.18341C16.1142 4.67226 13.8617 4.67226 11.6964 5.18341V9.80007H8.40483V6.37508C5.94 7.61913 3.86647 9.53444 2.41711 11.9059C0.967748 14.2774 0.199951 17.0111 0.199951 19.8C0.199951 22.589 0.967748 25.3227 2.41711 27.6942C3.86647 30.0657 5.94 31.981 8.40483 33.225V29.8H11.6964V34.4167C13.8617 34.9278 16.1142 34.9278 18.2795 34.4167V29.8H21.5711V33.225C24.0404 31.9844 26.1188 30.0705 27.5726 27.6988C29.0263 25.327 29.7977 22.5916 29.7999 19.8Z" fill="#CA9023"/>
              <path d="M18.2 40.0001H21.8V33.6001C20.6539 34.1113 19.4458 34.5006 18.2 34.76V40.0001Z" fill="#CA9023"/>
              <path d="M8.19995 40.0001H11.7999V34.76C10.5541 34.5006 9.34595 34.1113 8.19995 33.6001V40.0001Z" fill="#CA9023"/>
              <path d="M21.8 0H18.2V5.24005C19.4458 5.49951 20.6539 5.88878 21.8 6.4V0Z" fill="#CA9023"/>
              <path d="M11.7999 0H8.19995V6.4C9.34595 5.88878 10.5541 5.49951 11.7999 5.24005V0Z" fill="#CA9023"/>
            </svg>
          </div>
          <b>
            Get flag 7
            <span class="c-main-header__cabinet-menu-badge">
              <svg width="37" height="34" viewBox="0 0 37 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g filter="url(#filter1_d)">
                  <path d="M11 18.1557L10.7494 19.0883L18.2494 23.4327H18.7506L26.2506 19.0883L26.5 18.6557V16.4365L25.7494 16.0038L18.5 20.203L15.0825 18.223L14.8318 18.1557H11ZM18.7489 10.5664L18.2494 10.5673L10.7494 14.9111L10.5 15.3438V17.5635L11.2506 17.9962L18.5 13.797L21.8515 15.738L22.1021 15.8054H26L26.2489 14.8717L18.7489 10.5664Z" fill="#CA9023" stroke="white" stroke-linejoin="bevel"/>
                  <path d="M20.7703 15.5863L20.5191 15.654L19.5848 16.1969V13.9773L18.8342 13.5446L12.1103 17.4389L12.3608 18.3716H16.1933L16.4439 18.3042L17.4352 17.7301V20.0221L18.1874 20.4539L24.9269 16.518L24.6747 15.5863H20.7703Z" fill="#CA9023" stroke="white" stroke-linejoin="bevel"/>
                </g>
                <defs>
                  <filter id="filter1_d" x="0" y="0.132812" width="37" height="33.7326" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset/>
                    <feGaussianBlur stdDeviation="5"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0.967153 0 0 0 0 0.850097 0 0 0 0 0.674514 0 0 0 0.8 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                  </filter>
                </defs>
              </svg>
            </span>
          </b>
        </a>
      </li>
      <li>
        <a href="{{ route('cabinet.tools') }}">
          <div class="c-main-header__cabinet-menu-img">
            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M20.4368 19.3436C20.4368 19.3439 20.4365 19.344 20.4362 19.3438C20.4361 19.3438 20.436 19.3437 20.4359 19.3438L9.39155 25.3335C9.0874 25.4984 8.73189 25.2273 8.8043 24.8857L11.4338 12.4799C11.4339 12.4796 11.4342 12.4795 11.4344 12.4797C11.4344 12.4797 11.4345 12.4797 11.4346 12.4797C11.4347 12.4797 11.4347 12.4797 11.4348 12.4797L22.4791 6.49006C22.7832 6.3251 23.1387 6.59618 23.0663 6.93784L20.4368 19.3436ZM16.3387 12.7449C16.18 12.488 15.8106 12.488 15.6519 12.7449L14.8822 13.9906C14.8266 14.0806 14.7388 14.1452 14.6371 14.1709L13.2295 14.5274C12.9393 14.6009 12.8251 14.9567 13.0173 15.1889L13.9492 16.3153C14.0165 16.3967 14.05 16.5013 14.0428 16.6072L13.9426 18.0732C13.9219 18.3754 14.2207 18.5953 14.4981 18.482L15.8438 17.9324C15.941 17.8928 16.0496 17.8928 16.1468 17.9324L17.4925 18.482C17.7699 18.5953 18.0687 18.3754 18.048 18.0732L17.9478 16.6072C17.9406 16.5013 17.9741 16.3967 18.0414 16.3153L18.9733 15.1889C19.1655 14.9567 19.0513 14.6009 18.7611 14.5274L17.3535 14.1709C17.2518 14.1452 17.164 14.0806 17.1084 13.9906L16.3387 12.7449Z" fill="#CA9023"/>
              <path fill-rule="evenodd" clip-rule="evenodd" d="M16 4.92308C9.7894 4.92308 4.86076 9.94132 4.86076 16C4.86076 22.0587 9.7894 27.0769 16 27.0769C22.2106 27.0769 27.1392 22.0587 27.1392 16C27.1392 9.94133 22.2106 4.92308 16 4.92308ZM16 4.90651e-07C7.16345 -2.91769e-07 2.1438e-06 7.16344 1.38106e-06 16C6.18321e-07 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16345 24.8366 1.27307e-06 16 4.90651e-07Z" fill="#CA9023"/>
            </svg>
          </div>
          <b>Get tools</b>
        </a>
      </li>
    </ul>
    <div class="c-main-header__logout-icon">
      <a href="{{ route('logout') }}">
        <svg width="18" height="20" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.48182 2.3152C2.26364 3.7099 0 6.9735 0 10.795C0 15.8438 4.03636 20 9 20C13.9364 20 18 15.8438 18 10.795C18 6.9735 15.7364 3.7099 12.4909 2.3152V5.41144C14.1818 6.58298 15.3273 8.53557 15.3273 10.795C15.3273 14.3654 12.4909 17.2664 9 17.2664C5.50909 17.2664 2.67273 14.3654 2.67273 10.795C2.67273 8.53557 3.79091 6.58298 5.48182 5.41144V2.3152ZM7.5 10.7392V0H10.5V10.7392H7.5Z" fill="#CA9023"/>
        </svg>
      </a>
    </div>
    @include('components.user')
    <div class="c-authorization">
      @include('components.button', [
        'type' => 'link',
        'href' => route('logout'),
        'text' => 'Log out',
        'style' => 'gold',
        'size' => 'small',
      ])
    </div>
  @else
    <ul class="c-anchor-menu" uk-scrollspy-nav="closest: a; scroll: true">
      <li>
        <a href="#best-tools" uk-scroll>
          <b>Get tools</b>
        </a>
      </li>
      <li>
        <a href="{{ route('test.test') }}">
          <b>test</b>
        </a>
      </li>
    </ul>
    <nav class="c-main-navigation">
      <ul>
        <li><a href="{{ route('test.home') }}" class="c-link is-white"><b>About us</b></a></li>
        <li><a href="{{ route('test.test') }}" class="c-link is-white"><b>Test</b></a></li>
      </ul>
    </nav>
    <svg id="mobile-menu-icon" uk-toggle="target: #mobile-menu" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="3" y="5" width="18" height="2" fill="#C98F24"/>
      <rect x="3" y="11" width="18" height="2" fill="#C98F24"/>
      <rect x="3" y="17" width="18" height="2" fill="#C98F24"/>
    </svg>
    <div class="c-authorization">
      @guest
        <a href="{{ route('login') }}" class="c-link is-gold"><b>Log in</b></a>
        @include('components.button', [
          'type' => 'link',
          'href' => route('registration'),
          'text' => 'Register',
          'style' => 'gold',
          'size' => 'small',
        ])
      @endguest
      @auth
        @include('components.button', [
          'type' => 'link',
          'href' => route('cabinet.home'),
          'text' => 'Cabinet',
          'style' => 'gold',
          'size' => 'small',
        ])
      @endauth
    </div>
  @endif
</header>
