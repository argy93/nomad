<div
  class="c-who uk-transform-origin-center-left"
  uk-scrollspy="target: > div; cls:uk-animation-scale-up; delay: 500"
>
  <h2
    class="c-title is-gold"
    uk-scrollspy="cls:uk-animation-fade; delay: 500"
  >
    <b>Who is in Nomad Family?</b>
  </h2>
  <div class="c-who__row">
    @include('components.stopline')
    <div class="c-who__text">
      <p class="c-text is-white">
        Our family members do not aspire to become known, but there are well-known and influential people in our family.
      </p>
    </div>
  </div>
  <div class="c-who__row">
    @include('components.stopline')
    <div class="c-who__text">
      <p class="c-text is-white">
        Our family members may possibly quit their business, but a steady income never leaves them.
      </p>
    </div>
  </div>
  <div class="c-who__row">
    @include('components.stopline')
    <div class="c-who__text">
      <p class="c-text is-white">
        Members of our family can go to some secluded island in the ocean, but most likely that they will meet somebody of "ours" while surfing without even suspecting it.
      </p>
    </div>
  </div>
</div>
