<div class="c-criteria">
  <span class="c-criteria__bg is-active"></span>
  <div class="u-container">
    <div class="c-criteria__row">
      <div
        class="c-criteria__info"
        uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
      >
        <h2 class="c-title is-gold">
          <b>Selection criteria</b>
        </h2>
        <p class="c-text is-big is-white">
          We have strict yet simple selection criteria at Nomad Family. Meet more than 4 of them to join us:
        </p>
      </div>
      <div
        class="c-criteria__poll js-criteria-poll"
        uk-scrollspy="target: > div, a, button; cls:uk-animation-fade; delay: 400"
      >
        @include('components.checkbox', [
          'id' => 'poll-1',
          'label' => 'you would like to live in more than 3 countries'
        ])
        @include('components.checkbox', [
          'id' => 'poll-2',
          'label' => 'you are not afraid of frequent flights'
        ])
        @include('components.checkbox', [
          'id' => 'poll-3',
          'label' => 'you can read and understand more than 3 pages of text (and at least read the information on this website)'
        ])
        @include('components.checkbox', [
          'id' => 'poll-4',
          'label' => 'you are interested in new technologies'
        ])
        @include('components.checkbox', [
          'id' => 'poll-5',
          'label' => 'you are interested or you are not afraid of studying personal finances'
        ])
        @include('components.checkbox', [
          'id' => 'poll-6',
          'label' => 'you had business or investment experience'
        ])
        @include('components.checkbox', [
          'id' => 'poll-7',
          'label' => 'you have no criminal record'
        ])
        @include('components.button', [
          'type' => 'link',
          'href' => route('registration'),
          'buttonType' => 'button',
          'disabled' => true,
          'class' => 'js-criteria-btn',
          'style' => 'gold',
          'size' => 'normal',
          'text' => 'register and get 7th flag'
        ])
      </div>
    </div>
  </div>
</div>
