<div class="c-personal-card @if (isset($disabled) && $disabled === true) is-disabled @endif" style="background-image: url('{{ asset($background) }}');">
  <div class="c-personal-card__icon">
    <img src="{{ asset($icon) }}" alt="">
  </div>
  <p class="c-personal-card__title">{{ $title }}</p>
  @if (isset($disabled) && $disabled === true)
    <a href="javascript:" class="c-personal-card__link c-link is-white">Test passed</a>
  @else
    <a href="{{ 'cabinet/' . $href }}" class="c-personal-card__link c-link is-white">{{ $link }}</a>
  @endif
</div>
