<div class="c-meeting">
  <div class="u-container">
    <h2
      class="c-title is-gold"
      uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
    >
      <b>The meeting of the top country managers in Berlin</b>
    </h2>
    <div
      class="c-meeting__video"
      uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 800"
    >
      @include('components.video', ['src' => '/storage/common/nf-event.mp4'])
      <img
        class="c-meeting__square"
        src="{{ asset('/storage/common/circles-square-bg.svg') }}"
        alt=""
      >
    </div>
    <div
      class="is-desktop"
      uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 800"
    >
      <img src="{{ asset('/storage/common/circles-square-bg.svg') }}" alt="">
      @include('components.hs-slider', ['images' => [
      '/storage/common/slide-1-min.jpg',
      '/storage/common/slide-2-min.jpg',
      '/storage/common/slide-3-min.jpg',
      '/storage/common/slide-4-min.jpg',
      '/storage/common/slide-5-min.jpg',
      '/storage/common/slide-6-min.jpg',
      '/storage/common/slide-7-min.jpg',
      '/storage/common/slide-8-min.jpg',
      '/storage/common/slide-9-min.jpg',
      '/storage/common/slide-10-min.jpg',
      '/storage/common/slide-11-min.jpg',
      '/storage/common/slide-12-min.jpg',
      '/storage/common/slide-13-min.jpg',
      '/storage/common/slide-14-min.jpg',
      '/storage/common/slide-15-min.jpg',
      '/storage/common/slide-16-min.jpg',
      '/storage/common/slide-17-min.jpg',
      '/storage/common/slide-18-min.jpg',
      '/storage/common/slide-19-min.jpg',
      '/storage/common/slide-20-min.jpg',
      '/storage/common/slide-21-min.jpg',
      '/storage/common/slide-22-min.jpg',
      '/storage/common/slide-23-min.jpg',
      '/storage/common/slide-24-min.jpg',
      '/storage/common/slide-25-min.jpg'
        ]
      ])
    </div>
<!--
    <div class="c-meeting__slider">

    </div> -->
    <div class="is-mobile">
      <img class="c-meeting__square" src="{{ asset('/storage/common/circles-square-bg.svg') }}" alt="">
      @include('components.hs-slider', ['images' => [
        '/storage/common/slide-1-min.jpg',
        '/storage/common/slide-2-min.jpg',
        '/storage/common/slide-3-min.jpg',
        '/storage/common/slide-4-min.jpg',
        '/storage/common/slide-5-min.jpg',
        '/storage/common/slide-6-min.jpg',
        '/storage/common/slide-7-min.jpg',
        '/storage/common/slide-8-min.jpg',
        '/storage/common/slide-9-min.jpg',
        '/storage/common/slide-10-min.jpg',
        '/storage/common/slide-11-min.jpg',
        '/storage/common/slide-12-min.jpg',
        '/storage/common/slide-13-min.jpg',
        '/storage/common/slide-14-min.jpg',
        '/storage/common/slide-15-min.jpg',
        '/storage/common/slide-16-min.jpg',
        '/storage/common/slide-17-min.jpg',
        '/storage/common/slide-18-min.jpg',
        '/storage/common/slide-19-min.jpg',
        '/storage/common/slide-20-min.jpg',
        '/storage/common/slide-21-min.jpg',
        '/storage/common/slide-22-min.jpg',
        '/storage/common/slide-23-min.jpg',
        '/storage/common/slide-24-min.jpg',
        '/storage/common/slide-25-min.jpg'
        ],
        'videos' => [
          '/storage/common/nf-event.mp4'
        ]
      ])
      <div class="is-mobile__swipe">
        <span class="is-mobile__swipe-arrow is-left"></span>
        <span class="c-text">swipe to see more</span>
        <span class="is-mobile__swipe-arrow is-right"></span>
      </div>
    </div>
  </div>
</div>
