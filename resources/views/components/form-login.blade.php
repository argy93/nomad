<?php if (!isset($redirect)) $redirect = route('cabinet.home')  ?>

<form action="{{ route('login') }}" method="post" class="c-form-login">
  @csrf
  <input type="hidden" name="redirect" value="{{ $redirect }}">

  <div class="c-form-login__content">
    <div class="c-form-login__header">
      <p class="c-title is-white is-small"><b>Log in to your Nomad Family account</b></p>
      <p class="c-text is-white is-small uk-margin-top">Please enter your login and password to enter.</p>
    </div>
    <div class="c-form-login__body">
      @include('components.input', ['label' => 'Email', 'name' => 'email', 'style' => 'white', 'error' => count($errors)])
      @include('components.input', ['label' => 'Password', 'type' => 'password', 'name' => 'password', 'style' => 'white', 'error' => count($errors)])

      @if (count($errors))
        @include('components.error', [
          'content' => "<span class='c-text is-small is-white'>{$errors->first('email')}</span>"
        ])
      @endif

      <div class="c-form-login__buttons">
      <p class="c-text is-small is-white uk-margin-bottom">Have no account? <a href="<?php echo $redirect == route('cabinet.home') ? route('registration') : route('test.registration') ?>" class="c-link is-small is-white"><b>Register one now.</b></a></p>

      @include('components.button', [
        'style' => 'white',
        'type' => 'button',
        'buttonType' => 'submit',
        'arrowRight' => $redirect !== route('cabinet.home'),
        'text' => $redirect == route('cabinet.home') ? 'log in' : 'start the test'
      ])
      </div>
    </div>
  </div>
  <div class="c-form-login__logo">
    @include('components.logo')
  </div>
</form>
