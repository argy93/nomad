<?php
if (!isset( $class )) $class = '';
if (!isset( $type )) $type = 'link'; // link | button
if (!isset( $href )) $href = '#';
if (!isset( $buttonType )) $buttonType = 'button'; // button | submit
if (!isset( $style )) $style = 'gold'; // gold | white | empty | google
if (!isset( $size )) $size = 'normal'; // normal | medium | small
if (!isset( $arrowLeft )) $arrowLeft = false;
if (!isset( $arrowRight )) $arrowRight = false;
if (!isset( $text )) $text = 'no-text';
if (!isset( $disabled )) $disabled = false;
if (!isset( $full )) $full = false;

$full_class = $full ? 'is-full' : '';
$disabled_class = $disabled ? 'is-disabled' : '';
$class_name = "c-button is-$style is-$size $disabled_class $full_class $class";
?>
@if($type === 'link')
  <a href="{{ $href }}" class="{{ $class_name }}">
@elseif($type === 'button')
  <button type="{{ $buttonType }}" class="{{ $class_name }}">
@endif
  <span class="c-button__text">
    @if ($arrowLeft)
      <div class="c-button__arrow-prev">
        <svg width="24" height="10" viewBox="0 0 24 10" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M16.658 9.86C17.1113 8.65867 17.6667 7.62733 18.324 6.766H0.1V3.978H18.324C17.6893 3.11667 17.1453 2.08533 16.692 0.883999H19.276C20.6813 2.53867 22.2227 3.79667 23.9 4.658V6.12C22.2227 6.936 20.6813 8.18267 19.276 9.86H16.658Z" />
        </svg>
      </div>
    @endif
    @if ( $style === 'google' )
      <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path id="s1" d="M23 11.7645C23 10.9828 22.9353 10.1968 22.7974 9.4278H11.7317V13.8561H18.0685C17.8055 15.2843 16.9606 16.5477 15.7234 17.3506V20.2239H19.504C21.724 18.221 23 15.2632 23 11.7645Z" fill="#4285F4"/>
        <path id="s2" d="M11.7318 23C14.8959 23 17.5642 21.9817 19.5084 20.2239L15.7278 17.3506C14.676 18.052 13.3181 18.4492 11.7361 18.4492C8.67547 18.4492 6.0804 16.4252 5.14927 13.704H1.24805V16.666C3.23961 20.5492 7.29603 23 11.7318 23Z" fill="#34A853"/>
        <path id="s3" d="M5.14488 13.704C4.65345 12.2758 4.65345 10.7292 5.14488 9.30104V6.33899H1.24796C-0.415987 9.58837 -0.415987 13.4166 1.24796 16.666L5.14488 13.704Z" fill="#FBBC04"/>
        <path id="s4" d="M11.7318 4.5516C13.4044 4.52625 15.0209 5.14317 16.2322 6.2756L19.5817 2.99241C17.4608 1.04024 14.6459 -0.0330288 11.7318 0.000774945C7.29603 0.000774945 3.23961 2.45155 1.24805 6.33898L5.14496 9.30103C6.07177 6.5756 8.67116 4.5516 11.7318 4.5516Z" fill="#EA4335"/>
      </svg>
    @endif
    <b>{!! $text !!}</b>
    @if ($arrowRight)
      <div class="c-button__arrow-next">
        <svg width="24" height="10" viewBox="0 0 24 10" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M16.658 9.86C17.1113 8.65867 17.6667 7.62733 18.324 6.766H0.1V3.978H18.324C17.6893 3.11667 17.1453 2.08533 16.692 0.883999H19.276C20.6813 2.53867 22.2227 3.79667 23.9 4.658V6.12C22.2227 6.936 20.6813 8.18267 19.276 9.86H16.658Z" />
        </svg>
      </div>
    @endif
  </span>
@if($type === 'button')
  </button>
@elseif($type === 'link')
  </a>
@endif
