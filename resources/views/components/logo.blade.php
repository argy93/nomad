<?php
  if (!isset( $big )) $big = false;
  $mod_class = $big ? 'is-big' : '';
  $class_name = "c-logo $mod_class";
?>
<div class="{{ $class_name }}">
  <a href="/">
    @if (isset($big) && $big == true)
      <img src="{{ asset('/storage/common/logo-big.svg') }}" alt="">
    @else
      <img src="{{ asset('/storage/common/logo-small.svg') }}" alt="">
    @endif
  </a>
</div>
