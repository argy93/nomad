<div class="c-journey-box">
  <div
    class="c-journey-box__left"
    uk-scrollspy="cls:uk-animation-slide-right; delay: 1000"
  >
    <div class="c-journey-box__line">
      <span></span><span></span><span></span><span></span>
      <span></span><span></span><span></span><span></span>
      <span></span><span></span><span></span><span></span>
      <span></span><span></span><span></span><span></span>
    </div>
  </div>
  <div
    class="c-journey-box__center"
    uk-scrollspy="cls:uk-animation-scale-up; delay: 500"
  >
    <h2 class="c-title is-gold">
      <b>Start your new life journey</b>
    </h2>
    <p class="c-text is-big">
      Where can you live and how much can you earn?
    </p>
    @include('components.button', [
      'type' => 'link',
      'href' => route('test.test'),
      'buttonType' => 'button',
      'style' => 'gold',
      'size' => 'normal',
      'text' => 'Pass the test for free'
    ])
  </div>
  <div
    class="c-journey-box__right"
    uk-scrollspy="cls:uk-animation-slide-left; delay: 1000"
  >
    <div class="c-journey-box__line">
      <span></span><span></span><span></span><span></span>
      <span></span><span></span><span></span><span></span>
      <span></span><span></span><span></span><span></span>
      <span></span><span></span><span></span><span></span>
    </div>
  </div>
</div>
