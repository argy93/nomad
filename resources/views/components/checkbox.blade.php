<?php
  if ( !isset($id)) $id = '';
  if ( !isset($name)) $name = '';
  if ( !isset($type)) $type = 'checkbox';
  if ( !isset($checked)) $checked = false;
  if ( !isset($value)) $value = '';
?>

<label class="c-checkbox" for="{{ $id }}">
  <input
    @if ($checked) checked @endif
    id="{{ $id }}"
    type="{{ $type }}"
    name="{{ $name }}"
    value="{{ $value }}"
    class="c-checkbox__input" />
  <span class="c-checkbox__imitator"></span>
  @if ( isset($label) )
    <p class="c-checkbox__text">{{ $label }}</p>
  @endif
</label>
