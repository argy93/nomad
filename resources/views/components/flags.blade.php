<section class="c-flags">
  <h2 class="c-title is-gold">8 Flags of Nomad Family</h2>
  <p
    class="c-text is-big is-white"
    uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
  >
    You can receive the flags in a different order. It is more important to get the key ones.
  </p>
  <div class="c-flags__container">
    <div class="c-flags__swipe">
      <span
        class="c-text is-white"
        uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 1000"
      >
        swipe or tap on icon
      </span>
      <ul class="c-flags__icons js-flags-icons-slider">
        <li>
          <img src="{{ asset('/storage/common/citizenship.svg') }}" alt="">
        </li>
        <li>
          <img src="{{ asset('/storage/common/residency.svg') }}" alt="">
        </li>
        <li>
          <span class="is-yellow"></span>
          <img class="is-icon" src="{{ asset('/storage/common/business.svg') }}" alt="">
          <img class="is-icon__key" src="{{ asset('/storage/common/business-key.svg') }}" alt="">
        </li>
        <li>
          <img src="{{ asset('/storage/common/banking.svg') }}" alt="">
        </li>
        <li>
          <span class="is-yellow"></span>
          <img class="is-icon" src="{{ asset('/storage/common/place to live.svg') }}" alt="">
          <img class="is-icon__key" src="{{ asset('/storage/common/place to live-key.svg') }}" alt="">
        </li>
        <li>
          <img src="{{ asset('/storage/common/security.svg') }}" alt="">
        </li>
        <li>
          <span class="is-yellow"></span>
          <img class="is-icon" src="{{ asset('/storage/common/crypto.svg') }}" alt="">
          <img class="is-icon__key" src="{{ asset('/storage/common/crypto-key.svg') }}" alt="">
        </li>
        <li>
          <img src="{{ asset('/storage/common/consiousness.svg') }}" alt="">
        </li>
      </ul>
    </div>
    <div class="c-flags__flex js-flags-slider">
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '1',
          'img' => '/storage/common/flag-img.jpg',
          'name' => 'Citizenship',
          'text' => 'Second citizenship is effective insurance against what can happen in your home country. Today, there are more than 20 countries in the world that allow acquiring citizenship or residence permit. We help everyone in our community to go this way as quickly as possible!'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '2',
          'img' => '/storage/common/flag-img2.jpg',
          'name' => 'Residency',
          'text' => 'Everything is simple, choose a country with the lowest income tax and add a line to your passport.'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '3',
          'img' => '/storage/common/flag-img3.jpg',
          'name' => 'Business',
          'text' => 'Nowadays, there are enough businesses that do not require a constant presence. Nomad Family helps build this path and get one of the key flags.',
          'icon' => 'key'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '4',
          'img' => '/storage/common/flag-img4.jpg',
          'name' => 'Assets and Banking',
          'text' => 'Your bank accounts, savings, and cash flow should be in reliable jurisdictions.'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '5',
          'img' => '/storage/common/flag-img5.jpg',
          'name' => 'Countries for life',
          'text' => 'Imagine that you are not limited to geography and have a stable income, without even working in the traditional meaning. You are a tourist who has money, which means that you will be welcome everywhere.',
          'icon' => 'key'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '6',
          'img' => '/storage/common/flag-img6.jpg',
          'name' => 'Information Security',
          'text' => 'Today, the security of your personal and commercial data is no less important than physical.'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '7',
          'img' => '/storage/common/flag-img7.jpg',
          'name' => 'Digital Money',
          'text' => 'Thanks to the tools of Bitsonar, it is possible to ensure continuous growth for the members of the Nomad Family. It is one of the key flags, reaching which you can use all the other flags.',
          'icon' => 'zip'
        ])
      </div>
      <div class="c-flags__col">
        @include('components.flag', [
          'number' => '8',
          'img' => '/storage/common/flag-img8.jpg',
          'name' => 'Consciousness',
          'text' => 'Strengthen your mental force with practice and get prepared for the challenges and experiences that you could not even think of before.'
        ])
      </div>
    </div>
  </div>
</section>
