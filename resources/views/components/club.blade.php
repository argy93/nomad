<div class="c-club">
  <div class="c-club__info">
    <div
      class="c-club__info-text"
      uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
    >
      <h2 class="c-title is-gold">
        <b>Why are Nomad Family members more successful than others and how to join a club?</b>
      </h2>
      <p class="c-text is-big">
        The members of our community share the same DNA. DNA of successful people, DNA of leaders, absolutely independent and free in their choice. It is for them to decide where to go and what to do with their lives! Joining Nomad Family, you'll leverage your potentials hidden before!
      </p>
    </div>
  </div>
  <div
    class="c-club__img uk-transform-origin-bottom-right"
    uk-scrollspy="cls:uk-animation-scale-up; delay: 1000"
  >
    <img
      src="{{ asset('/storage/common/club-bg@1x.png') }}"
      srcset="{{ srcset('/storage/common/club-bg@4x.png', 4) }}"
      alt=""
    >
  </div>
</div>
