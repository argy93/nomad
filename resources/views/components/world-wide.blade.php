<div class="c-world-wide">
  <div class="uk-container">
    <h2
      class="c-title is-gold"
      uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 500"
    >
      <b>Thousands of successful Nomad Family<br> members worldwide</b>
    </h2>
    <div
      class="c-world-wide__content"
      uk-scrollspy="cls: uk-animation-fade; delay: 800"
    >
      <div class="c-world-wide__map">
        @include('components.map')
        @include('components.members-counter', ['value' => 18235])
      </div>
      <div class="c-world-wide__counter">
        @include('components.members-counter', ['value' => 18235])
        @include('components.button', [
        'style' => 'gold',
        'size' => 'medium',
        'href' => route('test.registration'),
        'text' => 'Start your<br> journey now'
      ])
      </div>
    </div>
  </div>
</div>
