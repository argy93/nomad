<?php if (!isset($cabinet)) $cabinet = false ?>
<?php
$class = '';

if ($cabinet) $class .= 'is-cabinet';
?>


<div class="c-form-login is-submit {{ $class }}">
  <div class="c-form-login__content">
    <div class="c-form-login__header">
      <img src="{{ asset('/storage/common/submit-great.svg') }}" alt="">
      <h2 class="c-title is-white">Great!</h2>
    </div>
    <div class="c-form-login__body">
      <p class="c-text is-white">You have successfully finished the test and we have already sent you an e-mail with the
        results. Go, check it out!</p>

      <div class="c-form-login__buttons justify-content-center">
        @include('components.button', [
          'style' => 'empty',
          'href' => route('home'),
          'text' => 'Go to homepage'
        ])
      </div>
    </div>
  </div>
</div>
