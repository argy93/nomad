<div class="c-fillup">
  <div class="c-fillup__features">
    <span id="feature-1" class="c-fillup__features-item"></span>
    <span id="feature-2" class="c-fillup__features-item"></span>
    <span id="feature-3" class="c-fillup__features-item"></span>
    <span id="feature-4" class="c-fillup__features-item"></span>
    <span id="feature-5" class="c-fillup__features-item"></span>
    <span id="feature-6" class="c-fillup__features-item"></span>
    <span id="feature-7" class="c-fillup__features-item"></span>
    <span id="feature-8" class="c-fillup__features-item"></span>
  </div>
  <div class="u-container c-fillup__content">
    <div class="c-fillup__gap"></div>
    <div
      class="c-fillup__title"
      uk-scrollspy="cls:uk-animation-slide-right-medium; delay: 500"
    >
      <p class="c-title is-white">
        <b>We'll help you achieve<br> any goal quickly.</b>
      </p>
    </div>
  </div>
</div>
