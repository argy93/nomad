<div class="c-theory js-tab">
  <div class="u-container">
    <h2
      class="c-title is-gold"
      uk-scrollspy="cls:uk-animation-fade; delay: 500"
    >
      <b>We use the system of the wealthiest people in the world — <br>“The theory of Flags”</b>
    </h2>
    <div class="c-theory__navigation">
      <button class="is-active" type="button" data-target="concept">Concept</button>
      <button type="button" data-target="goals">Goals</button>
      <button type="button" data-target="benefits">Benefits</button>
    </div>
    <div class="row is-active" data-type="flex" data-tab="concept">
      <div class="c-theory__col">
        <div uk-scrollspy="cls:uk-animation-slide-left-medium; delay: 800">
          @component('components.line-card')
            <p class="c-theory__tab-title c-text is-big is-gold"><b>Concept</b></p>
            <p class="c-text is-white">
              The system is based on the Flag Theory, devised more than 50 years ago by Harry Schultz, the highest-paid investment adviser in the world! We completely modernized it and added everything that works today and will work for years to come!
            </p>
            <p class="c-text is-gold uk-text-uppercase"><b>Nomad Family's version contains 8 flags!</b></p>
          @endcomponent
        </div>
      </div>
      <div class="c-theory__col">
        <div uk-scrollspy="cls:uk-animation-fade; delay: 500">
          @include('components.video', ['src' => '/storage/common/nf-teaser.mp4'])
        </div>
      </div>
    </div>
    <div class="row is-reverse" data-type="flex" data-tab="goals">
      <div class="c-theory__col">
        <div uk-scrollspy="cls:uk-animation-slide-right-medium; delay: 800">
          @component('components.line-card', ['align' => 'right'])
            <p class="c-theory__tab-title c-text is-big is-gold"><b>Goals</b></p>
            <p class="c-text is-white">The objective of the system is to help you live in those countries where you feel the best! Each time you leave a piece of your life in a new state, you get a flag. The goal is to diversify your personal and financial affairs so that no one could control you!</p>
          @endcomponent
        </div>
      </div>
      <div class="c-theory__col">
        <div uk-scrollspy="cls:uk-animation-scale-up; delay: 500">
          @include('components.ballons')
        </div>
      </div>
    </div>
    <div class="row" data-type="flex" data-tab="benefits">
      <div class="c-theory__col">
        <div uk-scrollspy="cls:uk-animation-slide-left-medium; delay: 800">
          @component('components.line-card')
            <p class="c-theory__tab-title c-text is-big is-gold"><b>Benefits</b></p>
            <p class="c-text is-white">Getting benefits in different countries helps to become free in the modern world of control and invisible, but extremely tight frameworks.
            When a member of the Nomad Family manages to distribute his flags in 8 directions in the world, he gets absolute freedom and independence in planning a productive life.</p>
          @endcomponent
        </div>
      </div>
      <div class="c-theory__col is-center">
        <div uk-scrollspy="cls:uk-animation-scale-up; delay: 500">
          @include('components.spiral-gold')
        </div>
      </div>
    </div>
  </div>
</div>
