<div class="c-features-centered">
  <h2 class="c-title is-white">
    <b>Why Nomad Family</b>
  </h2>
  <div class="u-container">
    <div class="c-features-centered__row js-features-centered-slider">
      <div class="c-features-centered__item is-col-6">
        @include('components.card', [
          'text' => 'We help with business',
          'img' => '/storage/common/business-gold.svg',
          'align' => 'center'
        ])
      </div>
      <div class="c-features-centered__item is-col-6">
        @include('components.card', [
          'text' => 'We help with a new residency',
          'img' => '/storage/common/residency-gold.svg',
          'align' => 'center'
        ])
      </div>
      <div class="c-features-centered__item is-col-4">
        @include('components.card', [
          'text' => 'Credible system verified over 50 years',
          'img' => '/storage/common/credible-icon-gold.svg',
          'align' => 'center'
        ])
      </div>
      <div class="c-features-centered__item is-col-4">
        @include('components.card', [
          'text' => 'You get the support<br>of an influential global community',
          'img' => '/storage/common/assistance-icon-gold.svg',
          'align' => 'center'
        ])
      </div>
      <div class="c-features-centered__item is-col-4">
        @include('components.card', [
          'text' => 'Innovative products:<br> Bitsonar, 8 Flags Theory',
          'img' => '/storage/common/bitsonar-icon-gold.svg',
          'align' => 'center'
        ])
      </div>
    </div>
    <div class="c-features-centered__dots"></div>
    @include('components.button', [
      'type' => 'button',
      'buttonType' => 'button',
      'style' => 'gold',
      'size' => 'normal',
      'text' => 'Join the family',
      'type' => 'link',
      'href' => route('registration')
    ])
  </div>
</div>
