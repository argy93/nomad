<div class="c-best-tools-cabinet">
  <div class="c-best-tools-cabinet__head">
    <div class="c-best-tools-cabinet__head-img">
      <img src="{{ asset('/storage/common/best-tools-number.svg') }}" alt="">
    </div>
    <p class="c-best-tools-cabinet__head-title">
      ever best tools to live anywhere
    </p>
  </div>
  <h3 class="c-title is-small is-white">
    <b>Choose the best cities and build your online business</b>
  </h3>
  @include('components.button', [
    'type' => 'link',
    'href' => route('cabinet.tools-success'),
    'style' => 'gold',
    'size' => 'normal',
    'text' => 'get 30 best tools for free'
  ])
</div>
