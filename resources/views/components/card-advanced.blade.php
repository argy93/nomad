<div class="c-card-advanced" >
  <div class="c-card-advanced__head" style="background-image: url('{{ asset('/storage/common/map-golden.png') }}');">
    <span class="c-title is-large is-white">{{ $number }}</span>
    <p class="c-text is-big is-white">{{ $text }}</p>
  </div>
  <div class="c-card-advanced__bottom">
    <div class="c-card-advanced__row">
      <p class="c-text is-big is-white">{{ $desc }}</p>
      @include('components.button', [
        'style' => 'white',
        'size' => 'medium',
        'arrowRight' => true,
        'text' => 'Let’s begin',
        'type' => 'link',
        'href' => route('test.test')
      ])
    </div>
  </div>
</div>
