<form action="" class="c-form-login">
  <div class="c-form-login__content">
    <div class="c-form-login__header">
      <p class="c-text is-big">Password recovery</p>
      <p>Enter your email adress below to get instructions for setting a new password.</p>
    </div>
    <div class="c-form-login__body">

      @include('components.input', ['label' => 'login', 'style' => 'white', 'error' => false])


      <div class="c-form-login__buttons">
      @include('components.button', [
        'style' => 'white',
        'size' => 'medium',
        'arrowRight' => true,
        'text' => 'Reset password'
      ])
      </div>
    </div>
  </div>
  <div class="c-form-login__logo">
    @include('components.logo')
  </div>
</form>
