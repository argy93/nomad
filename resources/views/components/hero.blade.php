<div class="c-hero" id="hero">
  <div class="c-hero__col is-box">
    <div class="c-hero__box">
      <video
        class="c-hero__video"
        src="{{ asset($video) }}"
        uk-video="autoplay: inview"
        poster="{{ asset('/storage/common/hero-video-bg.jpg') }}"
        loop
        muted
        playsinline
        uk-scrollspy="cls: uk-animation-fade; delay: 2000"
      ></video>
      <h1
        class="c-hero__title c-title is-white is-large"
        uk-scrollspy="cls: uk-animation-slide-bottom; delay: 2500"
      >
        <b>Limitless<span>.</span><br> Secure<span>.</span><br> Wealthy<span>.</span></b>
      </h1>
    </div>
    <div
      class="c-hero__spinner"
      uk-scrollspy="cls: uk-animation-fade; delay: 3000"
    >
      @include('components.spinner')
    </div>
  </div>
  <div class="c-hero__col is-content">
    <div
      class="c-hero__content"
      uk-scrollspy="target: > p,div; cls:uk-animation-slide-bottom-medium; delay: 500"
    >
      <p class="c-hero__text uk-margin-medium-bottom c-text is-white is-big">
        {{$text}}
      </p>
      <div style="opacity: 0">
        @include('components.button', [
          'text' => 'Join for free',
          'href' => route('test.registration')
        ])
      </div>
    </div>
  </div>
</div>
