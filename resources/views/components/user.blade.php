<div class="c-user">
  <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M21.9717 25C20.64 20.9356 16.8159 18 12.3063 18C7.79667 18 3.97267 20.9356 2.64095 25H0.554199C1.94858 19.8159 6.68183 16 12.3063 16C17.9308 16 22.664 19.8159 24.0584 25H21.9717Z" fill="#CA9023"/>
    <circle cx="12.5" cy="6.5" r="5.5" stroke="#CA9023" stroke-width="2"/>
  </svg>
  <div class="c-user__text">
    <span class="c-user__text-company">Nomad</span>
    <span class="c-user__text-name">{{ Auth::user()->name }}</span>
  </div>
</div>
