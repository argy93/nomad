<?php if (!isset($name)) $name = ''; ?>
<?php if (!isset($value)) $value = ''; ?>
<?php if (!isset($class)) $class = ''; ?>

<div class="c-range js-range-wrap {{ $class }}">
  <div class="c-range__flex">
    <input class="c-range__input js-range" name="{{ $name }}" type="range" min="{{ $min }}" max="{{ $max }}" step="{{ $step }}" value="{{ $value }}">
    <span class="c-range__cy">USD</span>
  </div>

  <p class="c-range__value js-range-value">100</p>
</div>
