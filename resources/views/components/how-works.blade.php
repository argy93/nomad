<div class="c-how-works">
  <div class="c-how-works__info">
    <div
      class="c-how-works__info-text"
      uk-scrollspy="target: > h2,p; cls:uk-animation-slide-bottom-medium; delay: 300"
    >
      <h2 class="c-title is-gold">
        <b>HOW THE 8 FLAG SYSTEM WORKS</b>
      </h2>
      <p class="c-text is-gold">
        <b>Reaching Flag 7 — you get all 8!</b>
      </p>
      <p class="c-text">
        Once joining Nomad Family, your life will no longer be the same. We are your point of no return. Make your desires and dreams a take-off runway for success. Nomad Family is a plane that helps you set your 8 flags around the world!
      </p>
      <div
        class="c-how-works__info-button"
        uk-scrollspy="cls:uk-animation-fade; delay: 1200"
      >
        <p class="c-text is-gold">
          <b>Break the limits!</b>
        </p>
        @include('components.button', [
          'type' => 'link',
          'href' => route('registration'),
          'buttonType' => 'button',
          'style' => 'gold',
          'size' => 'normal',
          'text' => 'Get flag 7'
        ])
      </div>
    </div>
  </div>
  <div
    class="uk-transform-origin-bottom-left"
    uk-scrollspy="cls:uk-animation-scale-up; delay: 1500"
  >
    @include('components.flag-progress')
  </div>
</div>
