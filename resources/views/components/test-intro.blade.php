<div class="c-test-intro">
  <div class="c-test-intro__head">
    @include('components.card-advanced', ['number' => number_format(get_users_counter(), 0, '', ','), 'text' => 'We have helped THIS MANY people take the next steps to a new life', 'desc' => 'Pass the 6 simple questions and get lifetime plan and advices'])
  </div>
  <div class="c-test-intro__bottom">
    <div class="c-test-intro__bottom-dots">
      <span></span>
      <span></span>
      <span></span>
    </div>
    @include('components.features-centered')
  </div>
</div>
