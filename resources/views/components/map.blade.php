<div class="c-map js-map">
  <img src="{{ asset('/storage/common/map.png') }}" alt="" srcset="{{ srcset('/storage/common/map@3x.png', 3) }}">
</div>
<script id="names-list">
    {!!  json_encode(Lang::get('names.list'))  !!}
</script>
<script id="member-template" type="text/template">
  <div class="c-map__data">
    <div class="c-map__user">
      <span class="c-map__user-marker"></span>
      <p class="c-map__user-info c-text is-white is-small">
        <b></b>
        <span>just joined family</span>
      </p>
    </div>
  </div>
</script>
