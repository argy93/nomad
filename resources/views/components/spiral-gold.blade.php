<?php
  if (!isset( $size )) $size = 389;

  $width = $size;
  $height = ($size * 96.4) / 100;
?>
<div class="c-spiral-gold is-active">
  <img src="{{ asset('/storage/common/golden-spiral-animation-w-auto@1x.png') }}" width="{{ $width }}" height="{{ $height }}" srcset="{{ srcset('/storage/common/golden-spiral-animation@4x.png', 4) }}" alt="">
</div>
