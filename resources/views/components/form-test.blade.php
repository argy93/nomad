<?php if (!isset($cabinet)) $cabinet = false ?>
<?php if (!isset($redirect)) $redirect = '' ?>
<?php
  $class = '';

  if ($cabinet) $class .= 'is-cabinet';
?>

<form action="{{ route('test.submit') }}" method="post" class="c-form-login no-flex is-white js-fs-container {{ $class }}">
  @csrf
  <input type="hidden" name="redirect" value="{{ $redirect }}">
  @if (count($errors->all()))
  <div class="c-error">
    @foreach ($errors->all() as $error)
      <div class="c-text is-white is-small">{{ $error }}</div>
    @endforeach
  </div>
  @endif
  <div class="is-tab-container">

    <div class="c-form-login__progress">
      <span class="c-form-login__tab is-active js-fs-tab-step c-text is-white is-big" data-id="#t1">1</span>
      <span class="c-form-login__tab js-fs-tab-step c-text is-white is-big" data-id="#t2">2</span>
      <span class="c-form-login__tab js-fs-tab-step c-text is-white is-big" data-id="#t3">3</span>
      <span class="c-form-login__tab js-fs-tab-step c-text is-white is-big" data-id="#t4">4</span>
      <span class="c-form-login__tab js-fs-tab-step c-text is-white is-big" data-id="#t5">5</span>
      <span class="c-form-login__tab js-fs-tab-step c-text is-white is-big" data-id="#t6">6</span>
    </div>

    <div class="is-tab-content">
      <div class="c-form-login__content js-fs-tab-body is-active" id="t2">
        <p class="c-text is-white is-big">Choose 3 countries you would like to live in</p>
        <div class="c-form-login__test">
          <div class="is-flex justify-content-between">
            <div class="c-form-login__col">
              <p class="c-text is-white is-small">First country</p>
              @include('components.dropdown', [
                'options' => get_countries_to_select(),
                'mod' => 'extended',
                'name' => 'first_country',
                'value' => old('first_country')
              ])
            </div>
            <div class="c-form-login__col">
              <p class="c-text is-white is-small">Second country</p>
              @include('components.dropdown', [
                'options' => get_countries_to_select(),
                'mod' => 'extended',
                'name' => 'second_country',
                'value' => old('second_country')
              ])
            </div>
            <div class="c-form-login__col">
              <p class="c-text is-white is-small">Third country</p>
              @include('components.dropdown', [
                'options' => get_countries_to_select(),
                'mod' => 'extended',
                'name' => 'third_country',
                'value' => old('third_country')
              ])
            </div>
          </div>

            <div class="c-form-login__buttons">
              <span class="is-right js-fs-button" data-tab="#t3">
                @include('components.button', [
                  'class' => 'js-fs-button-next',
                  'style' => 'white',
                  'size' => 'medium',
                  'arrowRight' => true,
                  'text' => 'next',
                  'href' => 'javascript:',
                  'disabled' => true
                ])
              </span>
            </div>

        </div>
      </div>

      <div class="c-form-login__content js-fs-tab-body" id="t3">
        <p class="c-text is-white is-big">Tell us your citizenship  </p>
        <div class="c-form-login__test">
          <p class="c-text is-white is-small">Citizenship</p>
          @include('components.dropdown', [
                'options' => get_countries_to_select(),
                'mod' => 'extended',
                'name' => 'citizenship'
              ])

          <div class="c-form-login__buttons ">
            <span class="js-fs-button" data-tab="#t2">
              @include('components.button', [
                'style' => 'white',
                'size' => 'medium',
                'arrowLeft' => true,
                'text' => 'back',
                'href' => 'javascript:',
              ])
            </span>
            <span class="js-fs-button" data-tab="#t4">
              @include('components.button', [
                'class' => 'js-fs-button-next',
                'style' => 'white',
                'size' => 'medium',
                'arrowRight' => true,
                'text' => 'next',
                'href' => 'javascript:',
                'disabled' => true,
              ])
            </span>
          </div>
        </div>
      </div>

      <div class="c-form-login__content js-fs-tab-body" id="t4">
        <p class="c-text is-white is-big">What is your current annual income?</p>
        <div class="c-form-login__test">
          @include('components.range', [
            'min' => '1000',
            'max' => '500000',
            'step' => '100',
            'name' => 'current_income',
            'value' => old('current_income')
          ])

          <div class="c-form-login__buttons ">
            <span class="js-fs-button" data-tab="#t3">
              @include('components.button', [
                'style' => 'white',
                'size' => 'medium',
                'arrowLeft' => true,
                'text' => 'back',
                'href' => 'javascript:',
              ])
            </span>
            <span class="js-fs-button" data-tab="#t5">
              @include('components.button', [
                'class' => 'js-fs-button-next',
                'style' => 'white',
                'size' => 'medium',
                'arrowRight' => true,
                'text' => 'next',
                'href' => 'javascript:',
              ])
            </span>
          </div>
        </div>
      </div>

      <div class="c-form-login__content js-fs-tab-body" id="t5">
        <p class="c-text is-white is-big">What is your desired annual income?</p>
        <div class="c-form-login__test">
          @include('components.range', [
            'min' => '1000',
            'max' => '500000',
            'step' => '100',
            'name' => 'annual_income',
            'value' => old('annual_income')
          ])

          <div class="c-form-login__buttons ">
            <span class="js-fs-button" data-tab="#t4">
              @include('components.button', [
                'style' => 'white',
                'size' => 'medium',
                'arrowLeft' => true,
                'text' => 'back',
                'href' => 'javascript:',
              ])
            </span>
            <span class="js-fs-button" data-tab="#t6">
              @include('components.button', [
                'class' => 'js-fs-button-next',
                'style' => 'white',
                'size' => 'medium',
                'arrowRight' => true,
                'text' => 'next',
                'href' => 'javascript:',
              ])
            </span>
          </div>
        </div>
      </div>

      <div class="c-form-login__content js-fs-tab-body" id="t6">
        <p class="c-text is-white is-big">Available investment</p>
        <div class="c-form-login__test">
          @include('components.range', [
            'min' => '1000',
            'max' => '500000',
            'step' => '100',
            'name' => 'available_investment',
            'value' => old('available_investment')
          ])

          <div class="c-form-login__buttons ">
            <span class="js-fs-button" data-tab="#t5">
              @include('components.button', [
                'class' => 'js-fs-button-next',
                'style' => 'white',
                'size' => 'medium',
                'arrowLeft' => true,
                'text' => 'back',
                'href' => 'javascript:',
              ])
            </span>
            <span class="js-fs-button" data-tab="#t7">
              @include('components.button', [
                'class' => 'js-fs-button-next',
                'style' => 'white',
                'size' => 'medium',
                'arrowRight' => true,
                'text' => 'next',
                'href' => 'javascript:',
              ])
            </span>
          </div>
        </div>
      </div>

      <div class="c-form-login__content js-fs-tab-body" id="t7">
        <p class="c-text is-white is-big">What is your family status?</p>
        <div class="c-form-login__test">
          <div class="is-flex">
            <div class="c-form-login__check">
              @include('components.checkbox', [
                'type' => 'radio',
                'name' => 'spouse',
                'id' => 'spouse',
                'label' => 'Spouse',
                'checked' => 'true',
                'value' => '1'
              ])
            </div>
            <div class="c-form-login__check">
              @include('components.checkbox', [
                'type' => 'radio',
                'name' => 'spouse',
                'id' => 'nospouse',
                'label' => 'No Spouse',
                'value' => '0'
              ])
            </div>
          </div>
          <div class="c-form-login__buttons ">
            <span class="js-fs-button" data-tab="#t6">
              @include('components.button', [
                'style' => 'white',
                'size' => 'medium',
                'arrowLeft' => true,
                'text' => 'back',
                'type' => 'button',
                'buttonType' => 'button',
              ])

            </span>
            <span>
              @include('components.button', [
                'class' => 'js-fs-button-next',
                'style' => 'white',
                'size' => 'medium',
                'type' => 'button',
                'buttonType' => 'submit',
                'arrowRight' => true,
                'text' => 'next',
              ])
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
