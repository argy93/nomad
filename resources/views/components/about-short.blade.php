<div class="c-about-short">
  <div class="c-about-short__content">
    <div
      class="c-about-short__content-info"
      uk-scrollspy="cls: uk-animation-slide-left-medium; delay: 1000"
    >
      <h2 class="c-title is-gold"><b>WE ARE<br> DIGITAL NOMADS</b></h2>
      <p class="c-text is-big is-white">
        Digital nomads are location-independent people who use technology to earn a living and, more important — live their best lives. Digital nomads work remotely, telecommuting rather than being physically present at a company's headquarters or office.
      </p>
    </div>
  </div>
  <div
    class="c-about-short__img"
    uk-scrollspy="cls: uk-animation-fade; delay: 500"
  >
    <img
      src="{{ asset('/storage/common/N-mask-image@1x.png') }}"
      srcset="{{ srcset('/storage/common/N-mask-image@4x.png', 4) }}"
      alt=""
    >
    <span></span>
  </div>
</div>
