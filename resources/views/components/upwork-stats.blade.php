<div
  class="c-upwork-stats"
  uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
>
  <img
    src="{{ asset('/storage/common/upwork-quote@1x.png') }}"
    srcset="{{ srcset('/storage/common/upwork-quote@4x.png', 4) }}"
    class="c-upwork-stats__img"
    alt=""
  />
  <div uk-scrollspy="cls:uk-animation-fade; delay: 300">
    @include('components.quote', ['mod' => 'gold', 'size' => 170])
  </div>
  <div class="c-upwork-stats__logo">
    <svg width="120" height="36" viewBox="0 0 120 36" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clip-path="url(#clip01)">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M76.6841 23.7714C73.5971 23.7714 71.0916 21.2082 71.0916 18.0474C71.0916 14.8866 73.5971 12.3246 76.6841 12.3246C79.7734 12.3246 82.2765 14.8866 82.2765 18.0474C82.2765 21.2082 79.7734 23.7714 76.6841 23.7714ZM76.6841 8.31543C71.4328 8.31543 67.1758 12.6738 67.1758 18.0474C67.1758 23.4234 71.4328 27.7806 76.6841 27.7806C81.9353 27.7806 86.1935 23.4234 86.1935 18.0474C86.1935 12.6738 81.9353 8.31543 76.6841 8.31543Z" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M97.7544 12.9855C95.0403 12.9855 92.8408 15.2367 92.8408 18.0135V27.2331H88.7702V8.8623H92.8408V11.6955C92.8408 11.6955 94.5737 8.8623 98.1425 8.8623H99.3888V12.9855H97.7544Z" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M56.4823 8.8623L59.5599 21.5775L62.9517 8.8623H66.993L61.7781 27.2331H57.7357L54.5432 14.4471L51.3495 27.2331H47.307L42.0921 8.8623H46.1346L49.5264 21.5775L52.6028 8.8623H56.4823Z" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M111.408 17.3352C114.301 15.6648 116.26 12.4932 116.26 8.8608H112.188C112.188 11.9232 109.754 14.4132 106.762 14.4132H106.217V0H102.146V27.2328H106.217V18.5808H106.702C107.101 18.5808 107.619 18.8508 107.854 19.1772L113.631 27.2328H118.51L111.408 17.3352Z" />
        <mask id="mask02" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="5" width="42" height="31">
          <path d="M0 5.58789H41.91V35.9995H0V5.58789Z" />
        </mask>
        <g mask="url(#mask02)">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M32.4132 22.8655C30.4857 22.8655 28.6814 22.0303 27.04 20.6719L27.4386 18.7483L27.4538 18.6739C27.8138 16.6315 28.9358 13.2043 32.4132 13.2043C35.0159 13.2043 37.1321 15.3715 37.1321 18.0355C37.1321 20.6983 35.0159 22.8655 32.4132 22.8655ZM32.4132 8.31549C27.9779 8.31549 24.5369 11.2615 23.1405 16.1191C21.0091 12.8407 19.3888 8.90229 18.4474 5.58789H13.6698V18.3019C13.6698 20.8111 11.6755 22.8523 9.22281 22.8523C6.7713 22.8523 4.77702 20.8111 4.77702 18.3019V5.58789H-0.000564575V18.3019C-0.000564575 23.5063 4.13688 27.7807 9.22281 27.7807C14.3087 27.7807 18.4474 23.5063 18.4474 18.3019V16.1743C19.3724 18.1507 20.512 20.1559 21.8954 21.9295L18.9726 35.9995H23.8569L25.9754 25.7935C27.8314 27.0079 29.9663 27.7807 32.4132 27.7807C37.6492 27.7807 41.9097 23.3935 41.9097 18.0355C41.9097 12.6763 37.6492 8.31549 32.4132 8.31549Z" />
        </g>
        <mask id="mask01" mask-type="alpha" maskUnits="userSpaceOnUse" x="117" y="8" width="4" height="4">
          <path d="M117.676 8.8623H120.021V11.2755H117.676V8.8623Z" />
        </mask>
        <g mask="url(#mask01)">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M118.909 10.0407C119.064 10.0407 119.152 9.95911 119.152 9.84151C119.152 9.71071 119.064 9.64111 118.909 9.64111H118.619V10.0407H118.909ZM118.39 9.4335H118.929C119.192 9.4335 119.388 9.56551 119.388 9.81991C119.388 10.0203 119.273 10.1451 119.111 10.1931L119.428 10.6551H119.152L118.868 10.2339H118.619V10.6551H118.39V9.4335ZM119.886 10.0683V10.0623C119.886 9.47551 119.442 8.9931 118.848 8.9931C118.262 8.9931 117.811 9.4827 117.811 10.0683V10.0755C117.811 10.6623 118.255 11.1447 118.848 11.1447C119.435 11.1447 119.886 10.6551 119.886 10.0683ZM117.676 10.0755V10.0683C117.676 9.4143 118.195 8.8623 118.848 8.8623C119.51 8.8623 120.022 9.40711 120.022 10.0623V10.0683C120.022 10.7235 119.503 11.2755 118.848 11.2755C118.188 11.2755 117.676 10.7307 117.676 10.0755Z" />
        </g>
      </g>
      <defs>
        <clipPath id="clip01">
          <rect width="120" height="36" fill="white"/>
        </clipPath>
      </defs>
    </svg>
  </div>
  <div class="c-upwork-stats__text c-upwork-stats__text-first">
    <span class="c-title is-white is-large">36%</span>
    <p class="c-text is-white is-big">of the working population in the USA are freelancers.</p>
  </div>
  <div class="c-upwork-stats__text c-upwork-stats__text-second">
    <div class="c-upwork-stats__text-header">
      <p class="c-title is-white is-large">$1,4</p>
      <span class="c-title is-white"><b>trillions</b></span>
    </div>
    <p class="c-text is-white">
      is contributed by them to the economy annually, an increase of almost 30% since last year.
    </p>
  </div>
</div>
