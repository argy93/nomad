<div class="c-social @if (isset($alignment) && $alignment === 'horisontal') is-flex @endif">
  <div class="c-social__item">
    <a href="#" target="_blank" class="c-link is-white">
      <img src="{{ asset('/storage/common/facebook.svg') }}" alt="">
      @if (!isset($alignment) || $alignment !== 'horisontal')
        <span >Facebook</span>
      @endif
    </a>
  </div>
  <div class="c-social__item">
    <a href="#" target="_blank" class="c-link is-white">
      <img src="{{ asset('/storage/common/twitter.svg') }}" alt="">
      @if (!isset($alignment) || $alignment !== 'horisontal')
        <span>Twitter</span>
      @endif
    </a>
  </div>
  <div class="c-social__item">
    <a href="#" target="_blank" class="c-link is-white">
      <img src="{{ asset('/storage/common/instagram.svg') }}" alt="">
      @if (!isset($alignment) || $alignment !== 'horisontal')
        <span>Instagram</span>
      @endif
    </a>
  </div>
  <div class="c-social__item">
    <a href="#" target="_blank" class="c-link is-white">
      <img src="{{ asset('/storage/common/medium.svg') }}" alt="">
      @if (!isset($alignment) || $alignment !== 'horisontal')
        <span>Medium</span>
      @endif
    </a>
  </div>
</div>
