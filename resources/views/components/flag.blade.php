<div class="c-flag">
  <div class="c-flag__container">
    @if (isset($icon))
    <div class="c-flag__key">
      @if ($icon == 'key')
        <img src="{{ asset('/storage/common/key.svg') }}" alt="">
      @elseif ($icon == 'zip')
        <img src="{{ asset('/storage/common/zip.svg') }}" alt="">
      @endif
      </div>
    @endif
    <div class="c-flag__img">
      <img src="{{ asset($img) }}
  " alt="">
    </div>
    <span class="c-flag__number">{{ $number }}</span>
    <div class="c-flag__text">

      <span class="c-flag__subtitle">{{ $name }}</span>
      <p>{{ $text }}</p>
    </div>
  </div>
</div>
