<div class="c-navigation js-navigation" uk-scrollspy-nav="closest: a; scroll: true">
  @foreach ($options as $item)
    <div class="c-navigation__item">
      <a href="{{ $item['anchor'] }}" uk-scroll class="c-navigation__flex">
        <span class="c-navigation__circle"></span>
        <span class="c-navigation__text">{{ $item['text'] }}</span>
      </a>
    </div>
  @endforeach
</div>
