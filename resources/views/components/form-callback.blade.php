<form action="" class="c-form-callback js-form-callback">
  <div class="c-form-callback__header">
    <img src="{{ asset('/storage/common/form-number.png') }}" alt="">
    <p class="c-form-callback__header-text">ever best tools to live anywhere</p>
  </div>
  <div class="c-form-callback__body">
    <p class="c-form-callback__body-desc">Choose the best cities and build your online business</p>
    @include('components.input', ['label' => 'Name', 'name' => 'name', 'style' => 'gold', 'error' => false, 'wide' => true])
    @include('components.input', ['label' => 'Email', 'name' => 'email', 'style' => 'gold', 'error' => false, 'wide' => true])
    @include('components.input', ['label' => 'Telegram (optional)', 'name' => 'telegram', 'style' => 'gold', 'error' => false, 'wide' => true])
    @include('components.button', [
      'text' => 'Get 30 best tools for free',
      'type' => 'button',
      'buttonType' => 'submit'
    ])
  </div>
</form>
