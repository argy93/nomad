<div class="c-profit">
  <h2
    class="c-title is-gold"
    uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 500"
  >
    <b>What do you get with us?</b>
  </h2>
  <div class="u-container">
    <div
      class="c-profit__row"
      uk-scrollspy="target: > div; cls:uk-animation-fade; delay: 500"
    >
      <div class="c-profit__item">
        @include('components.card', [
          'name' => 'Guideline',
          'text' => 'The guideline on how to get all 8 flags',
          'align' => 'center',
          'img' => '/storage/common/direction-icon.svg'
        ])
      </div>
      <div class="c-profit__item">
        @include('components.card', [
          'name' => 'Best tools',
          'text' => 'The best travel and relocation tools',
          'align' => 'center',
          'img' => '/storage/common/compass-icon.svg'
        ])
      </div>
      <div class="c-profit__item">
        @include('components.card', [
          'name' => 'Access to Flag 7',
          'text' => '+ access to Bitsonar service',
          'align' => 'center',
          'img' => '/storage/common/F7-access.svg'
        ])
      </div>
      <div class="c-profit__item">
        @include('components.card', [
          'name' => 'Assistance',
          'text' => 'Assistance from an influential community worldwide',
          'align' => 'center',
          'img' => '/storage/common/assistance-icon.svg'
        ])
      </div>
    </div>
    <div uk-scrollspy="cls:uk-animation-fade; delay: 1000">
      @include('components.button', [
        'type' => 'link',
        'href' => route('registration'),
        'buttonType' => 'button',
        'style' => 'gold',
        'size' => 'normal',
        'text' => 'I’m in!'
      ])
    </div>
  </div>
</div>
