<div class="c-footer">
  <div class="c-footer__row">
    <div class="c-footer__col c-footer__col-first">
      @include('components.logo', ['big' => true])
      <p class="c-text is-white is-small c-footer__copyright">
        Estonia at Harju maakond, Tallinn, Põhja-Tallinna linnaosa, Randla tn 13-201, 10315
      </p>
    </div>
    <div class="c-footer__col c-footer__col-second">
      <a href="{{ route('test.home') }}" class="c-link is-white c-footer__link">
        <b>About Nomad Family</b>
      </a>
      <a href="{{ route('test.test') }}" class="c-link is-white c-footer__link">
        <b>Test</b>
      </a>
    </div>
    <div class="c-footer__col c-footer__col-third">
      <a href="#" class="c-link is-white c-footer__link">
        <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M17 0.015625H1C0.734784 0.015625 0.48043 0.120982 0.292893 0.308518C0.105357 0.496055 0 0.750409 0 1.01562L0 17.0156C0 17.2808 0.105357 17.5352 0.292893 17.7227C0.48043 17.9103 0.734784 18.0156 1 18.0156H9.615V11.0556H7.2775V8.33062H9.615V6.33063C9.615 4.00563 11.035 2.74062 13.115 2.74062C13.8148 2.73858 14.5141 2.77447 15.21 2.84812V5.26562H13.77C12.645 5.26562 12.4275 5.80063 12.4275 6.58813V8.32312H15.125L14.775 11.0481H12.4275V18.0156H17C17.2652 18.0156 17.5196 17.9103 17.7071 17.7227C17.8946 17.5352 18 17.2808 18 17.0156V1.01562C18 0.750409 17.8946 0.496055 17.7071 0.308518C17.5196 0.120982 17.2652 0.015625 17 0.015625Z" fill="url(#paint0_radial)"/>
          <defs>
            <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(4.98125 34.479) rotate(-69.0842) scale(47.2344 20.6862)">
              <stop stop-color="#EDA334"/>
              <stop offset="0.656821" stop-color="#CD9121"/>
              <stop offset="1" stop-color="#B6862D"/>
            </radialGradient>
          </defs>
        </svg>
        <b>Facebook</b>
      </a>
      <a href="#" class="c-link is-white c-footer__link">
        <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M6.29 16.0156C13.837 16.0156 17.965 9.85911 17.965 4.52085C17.965 4.3456 17.965 4.17166 17.953 3.99838C18.756 3.42733 19.449 2.71832 20 1.90705C19.252 2.23392 18.457 2.44829 17.644 2.5428C18.5 2.03772 19.141 1.24422 19.448 0.307904C18.642 0.77852 17.761 1.11066 16.842 1.28887C15.288 -0.337617 12.689 -0.416421 11.036 1.11358C9.971 2.1001 9.518 3.57099 9.849 4.97398C6.55 4.81055 3.476 3.27663 1.392 0.753222C0.303 2.59926 0.86 4.95998 2.663 6.14538C2.01 6.12667 1.371 5.95358 0.8 5.64049V5.69176C0.801 7.61461 2.178 9.27027 4.092 9.65129C3.488 9.81374 2.854 9.8373 2.24 9.72013C2.777 11.3663 4.318 12.4938 6.073 12.5263C4.62 13.6507 2.825 14.2612 0.977 14.2593C0.651 14.2583 0.325 14.2395 0 14.2011C1.877 15.3866 4.06 16.0156 6.29 16.0127" fill="url(#paint0_radial)"/>
          <defs>
            <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(5.53472 30.6498) rotate(-64.4651) scale(43.4651 22.2026)">
              <stop stop-color="#EDA334"/>
              <stop offset="0.656821" stop-color="#CD9121"/>
              <stop offset="1" stop-color="#B6862D"/>
            </radialGradient>
          </defs>
        </svg>
        <b>Twitter</b>
      </a>
      <a href="#" class="c-link is-white c-footer__link">
        <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M5.28269 0.126081C3.81759 0.192186 2.54773 0.550384 1.52234 1.57144C0.493375 2.59787 0.139627 3.87276 0.0733576 5.32262C0.0321627 6.22755 -0.208723 13.0641 0.489794 14.857C0.960845 16.0665 1.8886 16.9965 3.10921 17.4691C3.67877 17.6906 4.32895 17.8406 5.28269 17.8844C13.2574 18.2453 16.2136 18.0488 17.4602 14.857C17.6814 14.2888 17.8336 13.6392 17.8757 12.6878C18.2402 4.69266 17.8166 2.95966 16.4267 1.57144C15.3243 0.471769 14.0276 -0.276804 5.28269 0.126081ZM5.35611 16.2764C4.48296 16.2371 4.00923 16.0916 3.69311 15.9692C2.89787 15.6602 2.30055 15.0653 1.99338 14.2747C1.46144 12.9124 1.63786 6.4421 1.68533 5.39512C1.73189 4.36959 1.93966 3.43233 2.66325 2.70875C3.55878 1.81543 4.7158 1.37762 12.5938 1.73316C13.6219 1.77961 14.5613 1.98695 15.2867 2.70875C16.1822 3.60206 16.6264 4.76799 16.2646 12.6158C16.2252 13.4868 16.0793 13.9594 15.9566 14.2747C15.1461 16.3517 13.2816 16.64 5.35611 16.2764ZM12.6807 4.23629C12.6807 4.82767 13.1616 5.30855 13.7553 5.30855C14.3491 5.30855 14.8309 4.82767 14.8309 4.23629C14.8309 3.64491 14.3491 3.16446 13.7553 3.16446C13.1616 3.16446 12.6807 3.64491 12.6807 4.23629ZM4.3764 9.00479C4.3764 11.5382 6.43524 13.5922 8.97497 13.5922C11.5147 13.5922 13.5735 11.5382 13.5735 9.00479C13.5735 6.47135 11.5147 4.41866 8.97497 4.41866C6.43524 4.41866 4.3764 6.47135 4.3764 9.00479ZM5.99015 9.00479C5.99015 7.3611 7.3263 6.0275 8.97497 6.0275C10.6237 6.0275 11.9598 7.3611 11.9598 9.00479C11.9598 10.6494 10.6237 11.9834 8.97497 11.9834C7.3263 11.9834 5.99015 10.6494 5.99015 9.00479Z" fill="url(#paint0_radial)"/>
          <defs>
            <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(4.98125 34.4792) rotate(-69.0842) scale(47.2346 20.6862)">
              <stop stop-color="#EDA334"/>
              <stop offset="0.656821" stop-color="#CD9121"/>
              <stop offset="1" stop-color="#B6862D"/>
            </radialGradient>
          </defs>
        </svg>
        <b>Instagram</b>
      </a>
      <a href="#" class="c-link is-white c-footer__link">
        <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0.015625H18V18.0156H0V0.015625ZM4.14293 5.64052C4.25378 5.74043 4.31038 5.88714 4.29536 6.03558V11.3802C4.32825 11.573 4.26747 11.7699 4.13164 11.9107L2.86144 13.4515V13.6546H6.46317V13.4515L5.19297 11.9107C5.05614 11.7702 4.9916 11.5745 5.01796 11.3802V6.75798L8.17935 13.6546H8.5463L11.2617 6.75798V12.255C11.2617 12.4017 11.2617 12.4299 11.1657 12.5259L10.1891 13.474V13.6772H14.9312V13.474L13.9884 12.5485C13.9052 12.485 13.8639 12.3808 13.8811 12.2776V5.47685C13.8639 5.37366 13.9052 5.26939 13.9884 5.20595L14.9538 4.28038V4.07721H11.6117L9.22938 10.0201L6.51962 4.07721H3.01387V4.28038L4.14293 5.64052Z" fill="url(#paint0_radial)"/>
          <defs>
            <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(4.98125 34.479) rotate(-69.0842) scale(47.2344 20.6862)">
              <stop stop-color="#EDA334"/>
              <stop offset="0.656821" stop-color="#CD9121"/>
              <stop offset="1" stop-color="#B6862D"/>
            </radialGradient>
          </defs>
        </svg>
        <b>Medium</b>
      </a>
    </div>
  </div>
</div>
