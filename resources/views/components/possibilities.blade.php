<div class="c-possibilities">
  <div class="c-possibilities__container">
    <div class="c-possibilities__row">
      <div
        class="c-possibilities__info c-possibilities__info-first"
        uk-scrollspy="cls: uk-animation-fade; delay: 800"
      >
        <h2 class="c-title is-gold c-possibilities__title">
          <b>Infinite possibilities</b>
        </h2>
        <p class="c-text c-possibilities__text">
          The world is tiny. From any corner of the world, you can travel to any other for a maximum of 24 hours.
        </p>
        <p class="c-text is-gold">
          <b>What is the point of staying in one place forever?</b>
        </p>
      </div>
      <div class="c-possibilities__img c-possibilities__img-first">
        <img
          src="{{ asset('/storage/common/infinity@1x.png') }}"
          srcset="{{ srcset('/storage/common/infinity@4x.png', 4) }}"
          uk-scrollspy="cls: uk-animation-scale-up; delay: 500"
          alt=""
        >
      </div>
    </div>
    <div class="c-possibilities__row">
      <div
        class="c-possibilities__img c-possibilities__img-second"
        uk-scrollspy="cls: uk-animation-slide-bottom-medium; delay: 500"
      >
        <img
          src="{{ asset('/storage/common/fireworks@1x.png') }}"
          srcset="{{ srcset('/storage/common/fireworks@4x.png', 4) }}"
          alt=""
        >
      </div>
      <div
        class="c-possibilities__info c-possibilities__info-second"
        uk-scrollspy="cls: uk-animation-slide-right-medium; delay: 800"
      >
        <p class="c-text c-possibilities__text">
          All you need to do is choose which countries you like and with Nomad Family learn how to enjoy their benefits. That's all you need to do to provide yourself with financial security and opportunities for growth, even if it seems complicated.
        </p>
        <p class="c-text is-gold">
          <b>Most countries accept new residents<br>or businesses very in a matter of days.</b>
        </p>
      </div>
    </div>
  </div>
</div>
