<?php if (!isset($name)) $name = "" ?>
<?php if (!isset($type)) $type = "text" ?>
<?php if (!isset($wide)) $wide = ""; else $wide = 'is-wide'; ?>
<label class="c-input is-{{ $style }} {{ $wide }} js-movable-placeholder @if ($error) is-error @endif">
  <input type="{{ $type }}" name="{{ $name }}" />
  <span>{{ $label }}</span>
</label>
