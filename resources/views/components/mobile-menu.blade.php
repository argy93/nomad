<div id="mobile-menu" class="mobile-menu" uk-offcanvas="mode: slide" style="display: none">
  <div class="mobile-menu__content uk-offcanvas-bar">
    <div class="mobile-menu__info">
      <div class="mobile-menu__info-img">
        <img src="{{ asset('/storage/common/mob-menu-bg.png') }}" alt="">
      </div>
      <div class="mobile-menu__info-header">
        @include('components.logo', ['big' => true])
        <button id="mobile-menu-close" class="uk-offcanvas-close" type="button" uk-close>
          <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="2.34314" y="0.928925" width="18" height="2" transform="rotate(45 2.34314 0.928925)" fill="#CA9023"/>
            <rect x="0.928925" y="13.6569" width="18" height="2" transform="rotate(-45 0.928925 13.6569)" fill="#CA9023"/>
          </svg>
        </button>
      </div>
      <ul class="mobile-menu__info-articles js-responsive-menu">
        <li><a href="/#best-tools" uk-scroll class="c-link is-small is-white">30 BEST  TOOLS</a></li>
        <li><a href="/#flag-theory" uk-scroll class="c-link is-small is-white">FLAG THEORY</a></li>
        <li><a href="/#how-it-work" uk-scroll class="c-link is-small is-white">HOW IT WORKS</a></li>
        <li><a href="/#events" uk-scroll class="c-link is-small is-white">EVENTS</a></li>
      </ul>
      <nav class="c-main-navigation">
        <ul>
          <li><a href="{{ route('test.home') }}" class="c-link is-white"><b>About us</b></a></li>
          <li><a href="{{ route('test.test') }}" class="c-link is-white"><b>Test</b></a></li>
        </ul>
      </nav>
      <div class="c-authorization">
        @guest
          <a href="{{ route('login') }}" class="c-link is-gold"><b>Log in</b></a>
          @include('components.button', [
            'type' => 'link',
            'href' => route('registration'),
            'text' => 'Register',
            'style' => 'gold',
            'size' => 'small',
          ])
        @endguest
        @auth
          @include('components.button', [
            'type' => 'link',
            'href' => route('cabinet.home'),
            'text' => 'Cabinet',
            'style' => 'gold',
            'size' => 'small',
          ])
        @endauth
      </div>
    </div>
    <div class="mobile-menu__social">
      <span class="mobile-menu__social-title">
        <b>visit our social media pages</b>
      </span>
      <div class="mobile-menu__social-icons">
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M17 0H1C0.734784 0 0.48043 0.105357 0.292893 0.292893C0.105357 0.48043 0 0.734784 0 1L0 17C0 17.2652 0.105357 17.5196 0.292893 17.7071C0.48043 17.8946 0.734784 18 1 18H9.615V11.04H7.2775V8.315H9.615V6.315C9.615 3.99 11.035 2.725 13.115 2.725C13.8148 2.72295 14.5141 2.75884 15.21 2.8325V5.25H13.77C12.645 5.25 12.4275 5.785 12.4275 6.5725V8.3075H15.125L14.775 11.0325H12.4275V18H17C17.2652 18 17.5196 17.8946 17.7071 17.7071C17.8946 17.5196 18 17.2652 18 17V1C18 0.734784 17.8946 0.48043 17.7071 0.292893C17.5196 0.105357 17.2652 0 17 0V0Z" fill="#8B8B8B"/>
        </svg>
        <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M5.661 14C12.4533 14 16.1685 8.61305 16.1685 3.94207C16.1685 3.78873 16.1685 3.63653 16.1577 3.48491C16.8804 2.98525 17.5041 2.36485 18 1.65499C17.3268 1.94101 16.6113 2.12858 15.8796 2.21127C16.65 1.76933 17.2269 1.07502 17.5032 0.255744C16.7778 0.667533 15.9849 0.958157 15.1578 1.11409C13.7592 -0.309087 11.4201 -0.37804 9.9324 0.960709C8.9739 1.82392 8.5662 3.11095 8.8641 4.33856C5.895 4.19556 3.1284 2.85338 1.2528 0.645398C0.2727 2.26068 0.774 4.32631 2.3967 5.36354C1.809 5.34717 1.2339 5.19571 0.72 4.92176V4.96662C0.7209 6.64911 1.9602 8.09781 3.6828 8.4312C3.1392 8.57335 2.5686 8.59396 2.016 8.49145C2.4993 9.93185 3.8862 10.9184 5.4657 10.9469C4.158 11.9307 2.5425 12.4649 0.8793 12.4632C0.5859 12.4623 0.2925 12.4459 0 12.4123C1.6893 13.4496 3.654 14 5.661 13.9974" fill="#8B8B8B"/>
        </svg>
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M5.28269 0.110456C3.81759 0.176561 2.54773 0.534759 1.52234 1.55582C0.493375 2.58224 0.139627 3.85714 0.0733576 5.30699C0.0321627 6.21192 -0.208723 13.0485 0.489794 14.8414C0.960845 16.0509 1.8886 16.9809 3.10921 17.4535C3.67877 17.675 4.32895 17.825 5.28269 17.8688C13.2574 18.2297 16.2136 18.0332 17.4602 14.8414C17.6814 14.2732 17.8336 13.6236 17.8757 12.6722C18.2402 4.67704 17.8166 2.94403 16.4267 1.55582C15.3243 0.456144 14.0276 -0.292429 5.28269 0.110456ZM5.35611 16.2608C4.48296 16.2215 4.00923 16.076 3.69311 15.9536C2.89787 15.6445 2.30055 15.0497 1.99338 14.2591C1.46144 12.8968 1.63786 6.42647 1.68533 5.3795C1.73189 4.35397 1.93966 3.4167 2.66325 2.69312C3.55878 1.7998 4.7158 1.36199 12.5938 1.71754C13.6219 1.76399 14.5613 1.97132 15.2867 2.69312C16.1822 3.58644 16.6264 4.75236 16.2646 12.6002C16.2252 13.4711 16.0793 13.9437 15.9566 14.2591C15.1461 16.336 13.2816 16.6244 5.35611 16.2608ZM12.6807 4.22066C12.6807 4.81204 13.1616 5.29293 13.7553 5.29293C14.3491 5.29293 14.8309 4.81204 14.8309 4.22066C14.8309 3.62929 14.3491 3.14884 13.7553 3.14884C13.1616 3.14884 12.6807 3.62929 12.6807 4.22066ZM4.3764 8.98917C4.3764 11.5226 6.43524 13.5766 8.97497 13.5766C11.5147 13.5766 13.5735 11.5226 13.5735 8.98917C13.5735 6.45572 11.5147 4.40304 8.97497 4.40304C6.43524 4.40304 4.3764 6.45572 4.3764 8.98917ZM5.99015 8.98917C5.99015 7.34547 7.3263 6.01187 8.97497 6.01187C10.6237 6.01187 11.9598 7.34547 11.9598 8.98917C11.9598 10.6338 10.6237 11.9678 8.97497 11.9678C7.3263 11.9678 5.99015 10.6338 5.99015 8.98917Z" fill="#8B8B8B"/>
        </svg>
      </div>
    </div>
  </div>
</div>
