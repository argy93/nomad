<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-personal-card</h3>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-6-6">
      @include('components.personal-card', [
        'icon' => '/storage/common/compass-icon.svg',
        'background' => '/storage/common/BITS-glowing-logo.svg',
        'title' => 'CHOOSE THE CITY AND BUILD YOUR ONLINE BUSINESS WITH THIS TOOLS',
        'href' => '#',
        'link' => 'Pass the test',
        'disabled' => true
      ])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.personal-card', [
        'icon' => '/storage/common/compass-icon.svg',
        'background' => '/storage/common/BITS-glowing-logo.svg',
        'title' => 'CHOOSE THE CITY AND BUILD YOUR ONLINE BUSINESS WITH THIS TOOLS',
        'href' => '#',
        'link' => 'Pass the test',
        'disabled' => true
      ])</code></pre>
</div>
