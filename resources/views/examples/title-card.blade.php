<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-title</h3>
  <p>Компонент имеет классы модификаторы:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-large </b>
        <p class="uk-width-5-6"> Увеличивает размер шрифта и высоту строки. <code>font-size: 96px; line-height: 124px</code></p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-small </b>
        <p class="uk-width-5-6"> Уменьшает размер шрифта и высоту строки. <code>font-size: 24px; line-height: 32px</code></p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-white </b>
        <p class="uk-width-5-6">
          Переопределяет цвет текста на белый. <br><code>color: #f4f4f4</code><br>
          Любой <b>span</b> внутри блока будет иметь жёлтый цвет текста. <br><code>color: #ca9023</code>
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-gold </b>
        <p class="uk-width-5-6">
          Переопределяет цвет текста на жёлтый. <br><code>color: #ca9023</code><br>
          Любой <b>span</b> внутри блока будет иметь белый цвет текста. <br><code>color: #f4f4f4</code>
        </p>
      </div>
    </li>
  </ul>
  @include('examples.title')
	<?php $content = file_get_contents( base_path() . '/resources/views/examples/title.blade.php') ?>
  <pre class="uk-text-danger"><code>{{ $content }}</code></pre>
</div>
