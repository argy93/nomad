<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-spiral-gold</h3>
  <p>Компонент имеет следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <div class="uk-width-1-6">size</div>
        <div class="uk-width-5-6">
          <code>Любое число</code><br>
          Задаёт иконке ширину и высоту. Если не передавать модификатор, то по умолчанию ширина иконки будет <b>389</b>, а высота <b>96%</b> от ширины.
        </div>
      </div>
    </li>
  </ul>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-5-6">
      @include('components.spiral-gold')
    </div>
    <div class="uk-width-1-6">
      @include('components.spiral-gold', ['size' => 100])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block">
    <code>&#64;include('components.spiral-gold')
&#64;include('components.spiral-gold', ['size' => 100])</code>
  </pre>
</div>
