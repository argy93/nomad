<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-navigation</h3>
  <p>Передается массив данных, массив данных содержит якорь '<b>anchor</b>' и название пункта навигации '<b>text</b>'</p>
  <br>

  @include('components.navigation', ['options' => [['anchor' => '#third', 'text' => 'HOW IT WORKS'], ['anchor' => '#second', 'text' => 'FLAG THEORY']]])
  <br><br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.navigation', ['options' =>
    [
      [
        'anchor' => '#third',
        'text' => 'HOW IT WORKS'
      ],
      [
        'anchor' => '#second',
        'text' => 'FLAG THEORY'
      ]
    ]
  ])</code></pre>
</div>
