<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-members-counter</h3>
  <p>Компонент имеет следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">value</b>
        <p class="uk-width-5-6">
          Принимает число, которое форматируется, отделяя каждые 3 символа запятой.
        </p>
      </div>
    </li>
  </ul>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-4">
      @include('components.members-counter', ['value' => 18235])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.members-counter', ['value' => 18235])</code></pre>
</div>
