<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-form-criteria</h3>
  @include('components.form-criteria')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.form-criteria')</code></pre>
</div>
