<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-card-advanced</h3>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-6-6">
      @include('components.card-advanced', [
        'number' => number_format(get_users_counter(), 0, '', ','),
        'text' => 'We have helped THIS MANY people take the next steps to a new life',
        'desc' => 'Pass the 6 simple questions and get lifetime plan and advices'
      ])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.card-advanced', [
        'number' => number_format(get_users_counter(), 0, '', ','),
        'text' => 'We have helped THIS MANY people take the next steps to a new life',
        'desc' => 'Pass the 6 simple questions and get lifetime plan and advices'
      ])</code></pre>
</div>
