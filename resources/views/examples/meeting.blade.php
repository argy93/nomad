<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-meeting</h3>
  @include('components.meeting')
  <br><br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.meeting')</code></pre>
</div>
