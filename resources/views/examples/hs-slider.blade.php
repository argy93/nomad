<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-hs-slider</h3>
  <p>Принимает в себя только путь к картинкам</p>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-6-6">
      @include('components.hs-slider', ['images' => [
          '/storage/common/hs-slide-1-min.jpg',
          '/storage/common/hs-slide-2-min.jpg',
          '/storage/common/hs-slide-3-min.jpg',
          '/storage/common/hs-slide-1-min.jpg'
        ]
      ])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>
    &#64;include('components.hs-slider', ['images' => [
      '/storage/common/hs-slide-1-min.jpg',
      '/storage/common/hs-slide-2-min.jpg',
      '/storage/common/hs-slide-3-min.jpg',
      '/storage/common/hs-slide-1-min.jpg'
    ]
  ])</code></pre>
</div>
