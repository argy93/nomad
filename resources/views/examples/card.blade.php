<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-card</h3>
  <p>Компонент принимает следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">name</b>
        <p class="uk-width-5-6">Название карточки</p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">text</b>
        <p class="uk-width-5-6">Сюда вписываем текст</p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">img</b>
        <p class="uk-width-5-6">Адрес иконки</p>
      </div>
    </li>
  </ul>
  <br>
  @include('components.card', [
    'name' => 'The choise',
    'text' => 'To live in a country where you want',
    'img' => '/storage/common/choice-icon.svg'
  ])
  <br><br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.card', [
  'name' => 'The choise',
  'text' => 'To live in a country where you want',
  'align' => 'center',
  'img' => '/storage/common/direction-icon.svg'
])</code></pre>
</div>
