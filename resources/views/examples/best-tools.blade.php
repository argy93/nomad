<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-best-tools</h3>
  @include('components.best-tools')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.best-tools')</code></pre>
</div>
