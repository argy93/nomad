<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-criteria</h3>
  @include('components.criteria')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.criteria')</code></pre>
</div>
