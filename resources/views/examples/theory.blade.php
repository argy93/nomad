<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-theory</h3>
  @include('components.theory')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.theory')</code></pre>
</div>
