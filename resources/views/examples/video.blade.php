<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-video</h3>
  @include('components.video', ['src' => '/storage/common/nf-event.mp4'])
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.video', ['src' => '/storage/public/common/nf-event.mp4'])</code></pre>
</div>
