<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-link</h3>
  <p>Компонент имеет классы модификаторы:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-small </b>
        <p class="uk-width-5-6">
          Уменьшает размер шрифта и высоту строки.<br>
          <code>font-size: 16px; line-height: 160%</code>
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-white </b>
        <p class="uk-width-5-6">
          Переопределяет цвет текста на белый. <br><code>color: #f4f4f4</code>
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">is-gold </b>
        <p class="uk-width-5-6">
          Переопределяет цвет текста на жёлтый. <br><code>color: #ca9023</code>
        </p>
      </div>
    </li>
  </ul>
  @include('examples.link')
	<?php $content = file_get_contents( base_path() . '/resources/views/examples/link.blade.php') ?>
  <pre class="uk-text-danger"><code>{{ $content }}</code></pre>
</div>
