<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-fillup</h3>
  @include('components.fillup')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.fillup')</code></pre>
</div>
