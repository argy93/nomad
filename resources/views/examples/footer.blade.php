<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-footer</h3>
  @include('components.footer')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.footer')</code></pre>
</div>
