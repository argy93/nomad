<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-flag</h3>
    <p>Компонент может принимать следующие параметры:</p>
    <ul>
      <li>
        <div class="uk-grid-small" uk-grid>
          <b class="uk-width-1-6">number</b>
          <p class="uk-width-5-6">Сюда вписываем число: 1, 2 или 3</p>
        </div>
      </li>
      <li>
        <div class="uk-grid-small" uk-grid>
          <b class="uk-width-1-6">img</b>
          <p class="uk-width-5-6">Записываем путь до картинки</p>
        </div>
      </li>
      <li>
        <div class="uk-grid-small" uk-grid>
          <b class="uk-width-1-6">name</b>
          <p class="uk-width-5-6">Название карточки (перед текстом)</p>
        </div>
      </li>
      <li>
        <div class="uk-grid-small" uk-grid>
          <b class="uk-width-1-6">text</b>
          <p class="uk-width-5-6">Текст выводящийся внутри карточки</p>
        </div>
      </li>
      <li>
        <div class="uk-grid-small" uk-grid>
          <b class="uk-width-1-6">icon</b>
          <div class="uk-width-5-6">
            <code>key | zip</code><br>
            Разные значения откроют разные картинки, если не добавлять переменную то выводиться не будет.
          </div>
        </div>
      </li>
    </ul>
    <br>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-3">
      @include('components.flag', [
        'number' => '1',
        'img' => '/storage/common/flag-img.jpg',
        'name' => 'Citizenship',
        'text' => 'Second citizenship is exceptionally effective insurance against what can ...',
        'icon' => 'key'
      ])
    </div>
    <div class="uk-width-1-3">
      @include('components.flag', [
        'number' => '2',
        'img' => '/storage/common/flag-img2.png',
        'name' => 'Citizenship',
        'text' => 'Second citizenship is exceptionally effective insurance against what can ...',
        'icon' => 'zip'
      ])
    </div>
    <div class="uk-width-1-3">
      @include('components.flag', [
        'number' => '3',
        'img' => '/storage/common/flag-img3.png',
        'name' => 'Citizenship',
        'text' => 'Second citizenship is exceptionally effective insurance against what can ...'
      ])
    </div>
  </div>
  <br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.flag', [
  'number' => '1',
  'img' => '/storage/common/flag-img.jpg',
  'name' => 'Citizenship',
  'text' => 'Second citizenship is exceptionally effective insurance against what can ...',
  'icon' => 'key'
])
</code></pre>
</div>
