<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-quote</h3>
  <p>Компонент имеет следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <div class="uk-width-1-6">mod</div>
        <div class="uk-width-5-6">
          <code>white | gold</code><br>
          Задаёт иконке белый или жёлтый цвет соответственно. Если не передавать модификатор, то по умолчанию будет выставлен белый цвет.
        </div>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <div class="uk-width-1-6">size</div>
        <div class="uk-width-5-6">
          <code>Любое число</code><br>
          Задаёт иконке ширину и высоту. Если не передавать модификатор, то по умолчанию ширина иконки будет <b>138</b>, а высота <b>94%</b> от ширины.
        </div>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <div class="uk-width-1-6">empty</div>
        <div class="uk-width-5-6">
          <code>true | false</code><br>
          Делает компонент прозрачным
        </div>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <div class="uk-width-1-6">border</div>
        <div class="uk-width-5-6">
          <code>Любое число</code><br>
          Управляет шириной обводки. По умолчанию значение обводки <b>0</b>.
        </div>
      </div>
    </li>
  </ul>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-3">
      @include('components.quote', ['size' => 112])
    </div>
    <div class="uk-width-1-3">
      @include('components.quote', ['mod' => 'gold'])
    </div>
    <div class="uk-width-1-3">
      @include('components.quote', ['mod' => 'gold', 'empty' => true, 'border' => 2])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block">
    <code>&#64;include('components.quote', ['size' => 112])
&#64;include('components.quote', ['mod' => 'gold'])
&#64;include('components.quote', ['mod' => 'gold', 'empty' => true, 'border' => 2])</code>
  </pre>
</div>
