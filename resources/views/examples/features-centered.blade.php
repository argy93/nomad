<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-features-centered</h3>
  @include('components.features-centered')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.features-centered')</code></pre>
</div>
