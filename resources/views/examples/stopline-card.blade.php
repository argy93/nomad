<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-stopline</h3>
  <p>Компонент выводит картинку stopline:</p>
  @include('components.stopline')
  <br><br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.stopline')
        </code></pre>
</div>
