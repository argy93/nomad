<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-line-card</h3>
  <p>Компонент может принимать только 1 параметр:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">align </b>
        <p class="uk-width-5-6">right<br> Если right, то выравнивание идет по правому краю</p>
      </div>
    </li>
    <li>Если ничего не передавать, то выравнивание происходит по левому краю</li>
  </ul>
  <br>
  @component('components.line-card', ['align' => 'right'])
    <p class="c-text is-big is-gold"><b>Concept</b></p>
    <p class="c-text is-white">The system is based on the Flag Theory, devised more than 50 years ago by Harry Schultz, the highest-paid investment adviser in the world! We completely modernized it and added everything that works today and will work for years to come!</p>
    <p class="c-text is-gold uk-text-uppercase"><b>Nomad Family's version contains 8 flags!</b></p>
  @endcomponent
  <br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&commat;component('components.line-card', ['align' => 'right'])
  &lt;p class="c-text is-big is-gold">&lt;b>Concept&lt;/b>&lt;/p>
  &lt;p class="c-text is-white">
    The system is based on the Flag Theory,
    devised more than 50 years ago by Harry Schultz,
    the highest-paid investment adviser in the world!
    We completely modernized it and added everything that
    works today and will work for years to come!
  &lt;/p>
  &lt;p class="c-text is-gold uk-text-uppercase">
      &lt;b>Nomad Family's version contains 8 flags!&lt;/b>
  &lt;/p>
&commat;endcomponent</code></pre>

</div>
