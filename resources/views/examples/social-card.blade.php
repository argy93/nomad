<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-social</h3>
  <p>Компонент может принимать следующие параметры:</p>
  <p>alignment: vertical | horizontal</p>
  <p>horizontal - убирает текст и выстраивает иконки в ряд</p>
  @include('components.social')
  <br>
  <br>
  @include('components.social', ['alignment' => 'horisontal'])
  <pre class="uk-text-danger uk-display-inline-block">
          <code>&#64;include('components.social')
&#64;include('components.social', ['alignment' => 'horisontal'])
          </code>
        </pre>
</div>
