<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-citation</h3>
  @include('components.citation')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.citation')</code></pre>
</div>
