<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-hero</h3>
  <p>Компонент выводит ошибку для полей ввода. В качестве аргумента, принимает строку текста или <b>HTML</b> код.</p>
  <br>
  @include('components.hero', ['title' => 'Limitless. Secure. Wealthy.', 'video' => '/storage/common/hero-video.mp4', 'text' => 'Nomad Family is a worldwide network that helps thousands of people to transform their lives and achieve goals by traveling, living in any country they want, and building businesses as easy as ABC.'])
  <br><br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.hero')</code></pre>
</div>
