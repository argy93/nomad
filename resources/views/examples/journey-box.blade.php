<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-journey-box</h3>
  @include('components.journey-box')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.journey-box')</code></pre>
</div>
