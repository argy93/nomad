<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-dropdown</h3>
  <p>Компонент может принимать следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">option</b>
        <div class="uk-width-5-6">
          <code>Массив</code><br>
          Передаёт массив стран, которые выводятся в тегах <b>option</b> и формируют выпадающий список.<br>
          Для получения локализированного массива стран, можно воспользоваться функцией:<br>
          <pre class="uk-text-danger uk-display-inline-block">
            <code>get_countries_to_select()</code>
          </pre>
        </div>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">mod</b>
        <p class="uk-width-5-6">
          <code>extended</code><br>
          Добавляет в выпадающий список поле для поиска.
        </p>
      </div>
    </li>
  </ul>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2">
      @include('components.dropdown', ['options' => get_countries_to_select()])
    </div>
    <div class="uk-width-1-2">
      @include('components.dropdown', ['mod' => 'extended', 'options' => get_countries_to_select()])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block">
    <code>$countries = [
  'value-1',
  'value-2',
  ... ,
  'value-n',
]
&#64;include('components.dropdown', ['options' => $countries])
&#64;include('components.dropdown', ['mod' => 'extended', 'options' => $countries])</code>
</pre>
</div>
