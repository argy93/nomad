<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-form</h3>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-2-5">
      @include('components.form-callback')
    </div>
    <div class="uk-width-3-5">
      @include('components.form-recovery')
    </div>
  </div>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-2-5">
      @include('components.form-login')
    </div>
    <div class="uk-width-3-5">
      @include('components.form-register')
    </div>
  </div>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-5-5">
      @include('components.form-test')
    </div>
  </div>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-2-5">
      @include('components.form-submit')
    </div>
    <div class="uk-width-3-5">

    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>
  &#64;include('components.form-callback')
  &#64;include('components.form-recovery')
  &#64;include('components.form-login')
  &#64;include('components.form-register')
  &#64;include('components.form-test')
  &#64;include('components.form-submit')</code></pre>
</div>
