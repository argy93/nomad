<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-club</h3>
  @include('components.club')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.club')</code></pre>
</div>
