<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-profit</h3>
  @include('components.profit')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.profit')</code></pre>
</div>
