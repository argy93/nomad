<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-checkbox</h3>
  <p>Компонент может принимать следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">type</b>
        <p class="uk-width-5-6">
          <code>checkbox | radio</code><br>
          Задаёт инпуту тип. По умолчанию выбран <b>checkbox</b>
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">label</b>
        <p class="uk-width-5-6">
          <code>Любая строка</code><br>
          Дабавляет в компонент <b>label</b>.
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">id</b>
        <p class="uk-width-5-6">
          <code>Любая строка или число</code><br>
          Добавляет инпуту идентификатор. Если используется совместно с параметром <b>label</b>, то связывает инпут с текстом.
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">name</b>
        <p class="uk-width-5-6">
          <code>Любая строка</code><br>
          Задаёт инпуту атрибут <b>name</b>. Если используется совместно с типом <b>radio</b>, то свзфвает между собой инпуты.
        </p>
      </div>
    </li>
  </ul>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2">
      @include('components.checkbox')
    </div>
    <div class="uk-width-1-2">
      @include('components.checkbox', ['id' => 'name', 'label' => 'First name'])
    </div>
  </div>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2">
      @include('components.checkbox', ['type' => 'radio', 'name' => 'radio-btn'])
    </div>
    <div class="uk-width-1-2">
      @include('components.checkbox', ['type' => 'radio', 'name' => 'radio-btn'])
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.checkbox')
&#64;include('components.checkbox', ['id' => 'name', 'label' => 'First name'])
&#64;include('components.checkbox', ['type' => 'radio', 'name' => 'radio-btn'])</code></pre>
</div>
