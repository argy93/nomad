<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-logo</h3>
  <p>Компонент может принимать следующие параметры:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">big </b>
        <p class="uk-width-5-6">true | false <br> Если true, то выводится логотип с текстом</p>
      </div>
    </li>
  </ul>
  @include('components.logo')
  <br>
  @include('components.logo', ['big' => true])

  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.logo')
&#64;include('components.logo', ['big' => true])
        </code></pre>
</div>
