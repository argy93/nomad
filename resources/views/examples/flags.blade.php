<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-flags</h3>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-6-6">
      @include('components.flags')
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.flags')</code></pre>
</div>
