<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-error</h3>
  <p>Компонент выводит ошибку для полей ввода. В качестве аргумента, принимает строку текста или <b>HTML</b> код.</p>
  <br>
	<?php $html = "
	        <p class='c-text is-small is-white'>Password or login is incorrect! Try again to proceed.</p>
          <p class='c-text is-small is-white'><a href='#'><b>Forgot password?</b></a></p>
	      " ?>
  @include('components.error', ['content' => $html])
  <br><br>
	<?php $content = file_get_contents( base_path() . '/resources/views/examples/error.blade.php') ?>
  <pre class="uk-text-danger uk-display-inline-block"><code>{{ $content }}</code></pre>
</div>
