<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-test-intro</h3>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-6-6">
      @include('components.test-intro')
    </div>
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.test-intro')</code></pre>
</div>
