<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-who</h3>
  @include('components.who')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.who')</code></pre>
</div>
