<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-travel</h3>
  @include('components.travel')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.travel')</code></pre>
</div>
