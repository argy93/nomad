<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-how-works</h3>
  @include('components.how-works')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.how-works')</code></pre>
</div>
