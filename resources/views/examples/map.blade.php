<div class="uk-card uk-card-body uk-card-secondary">
  <h3 class="uk-card-title">c-map</h3>
  <div style="max-width: 1100px; height: 545px; margin: 0 auto">
    @include('components.map')
  </div>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.map')</code></pre>
</div>
