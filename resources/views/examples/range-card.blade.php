<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-range</h3>
    <p>Компонент может принимать следующие параметры:</p>
    <ul>
      <li>
        <div class="uk-grid-small" uk-grid>
          <b class="uk-width-1-6">min </b>
          <p class="uk-width-5-6">Минимальные значения ползунка</p>
          <b class="uk-width-1-6">max</b>
          <p class="uk-width-5-6">Максимальные значения ползунка</p>
          <b class="uk-width-1-6">step</b>
          <p class="uk-width-5-6">Шаг с которым перемещается ползунок</p>
          <b class="uk-width-1-6">name</b>
          <p class="uk-width-5-6">Заполняется в поле name в input</p>
          <b class="uk-width-1-6">class</b>
          <p class="uk-width-5-6">Дополнительный класс-модификатор, нужен для перестилизации отдельного компонента</p>
        </div>
      </li>
    </ul>
  <br>
  @include('components.range', ['min' => '0', 'max' => '100', 'step' => '0.25', 'name' => 'first', 'class' => 'class-mod'])
  <br>
  @include('components.range', ['min' => '100', 'max' => '10000', 'step' => '0.25', 'name' => 'second', 'class' => 'class-mod2'])
  <br><br>
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.range', ['min' => '0', 'max' => '100', 'step' => '0.25', 'name' => 'first', 'class' => 'class-mod'])
&#64;include('components.range', ['min' => '100', 'max' => '10000', 'step' => '0.25', 'name' => 'second', 'class' => 'class-mod2'])  </code></pre>
</div>
