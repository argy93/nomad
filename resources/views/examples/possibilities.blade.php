<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-possibilities</h3>
  @include('components.possibilities')
  <pre class="uk-text-danger uk-display-inline-block uk-margin-large-top"><code>&#64;include('components.possibilities')</code></pre>
</div>
