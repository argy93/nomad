<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-button</h3>
  <p>Компонент имеет следующие параметры:</p>
  <ul>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">class</b>
        <p class="uk-width-5-6">
          Добавляет дополнительный класс
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">type</b>
        <p class="uk-width-5-6">
          <code>link | button</code> <br>
          Определяет html тип кнопки, (&lt;a> &lt;button>)
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">href</b>
        <p class="uk-width-5-6">
          добавляет содержимое в href. Работает только в комбинации со ссылкой
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">buttonType</b>
        <p class="uk-width-5-6">
          <code>button | submit</code> <br>
          определяет html тип кнопки (прим. <code>type="button"</code>)
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">style</b>
        <p class="uk-width-5-6">
          <code>gold | white | empty | google</code> <br>
          определяет внешний вид (стили) кнопки
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">size</b>
        <p class="uk-width-5-6">
          <code>normal | medium | small</code> <br>
          определяет размер кнопки
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">arrowLeft</b>
        <p class="uk-width-5-6">
          добавляет стрелку слева
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">arrowRight</b>
        <p class="uk-width-5-6">
          добавляет стрелку справа
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">text</b>
        <p class="uk-width-5-6">
          Меняет текст в кнопке
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">disabled</b>
        <p class="uk-width-5-6">
          Делает кнопку неактивной
        </p>
      </div>
    </li>
    <li class="uk-margin-bottom">
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">full</b>
        <p class="uk-width-5-6">
          Растягивает кнопку по всей ширине
        </p>
      </div>
    </li>
  </ul>
  <br><br>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'text' => 'Join for free'
])</code></pre>
      @include('components.button', [
        'text' => 'Join for free'
      ])
    </div>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'style' => 'white',
  'text' => 'Join for free'
])</code></pre>
      @include('components.button', [
        'style' => 'white',
        'text' => 'Join for free'
      ])
    </div>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'style' => 'empty',
  'text' => 'Join for free'
])</code></pre>
      @include('components.button', [
        'style' => 'empty',
        'text' => 'Join for free'
      ])
    </div>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'style' => 'google',
  'text' => 'Join for free'
])</code></pre>
      @include('components.button', [
        'style' => 'google',
        'text' => 'Join for free'
      ])
    </div>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'style' => 'white',
  'size' => 'medium',
  'arrowRight' => true,
  'text' => 'Let’s begin'
])</code></pre>
      <br>
      @include('components.button', [
        'style' => 'white',
        'size' => 'medium',
        'arrowRight' => true,
        'text' => 'Let’s begin'
      ])
    </div>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'style' => 'white',
  'size' => 'small',
  'arrowRight' => true,
  'text' => 'Next'
])</code></pre>
      <br>
      @include('components.button', [
        'style' => 'white',
        'size' => 'small',
        'arrowRight' => true,
        'text' => 'Next'
      ])
    </div>
    <div class="uk-width-1-2">
      <pre class="uk-text-danger uk-display-inline-block"><code>&commat;include('components.button', [
  'style' => 'white',
  'size' => 'small',
  'arrowRight' => true,
  'disabled' => true,
  'text' => 'Next'
])</code></pre>
      <br>
      @include('components.button', [
        'style' => 'white',
        'size' => 'small',
        'arrowRight' => true,
        'disabled' => true,
        'text' => 'Next'
      ])
    </div>
  </div>
</div>
