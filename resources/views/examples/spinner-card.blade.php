<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-spinner</h3>
  @include('components.spinner')
  <pre class="uk-text-danger"><code>&#64;include('components.spinner')</code></pre>
</div>
