<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-input</h3>
  <p>Для того чтобы вызвать скрипт, который передвигает текст вверх, необходимо использовать класс <b>js-movable-placeholder</b>
  </p>
  <p>Компонент имеет модификатор <b>label</b>, который принимает значения:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">"Любая строка"</b>
        <p class="uk-width-5-6">
          Позволяет передать строку текста, которая будет играть роль "<b>плейсхолдера</b>", в не активном состоянии поля ввода, и роль "<b>лейбла</b>" - в активном состоянии.
        </p>
      </div>
    </li>
  </ul>
  <p>Компонент имеет модификатор <b>style</b>, который принимает значения:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">white</b>
        <p class="uk-width-5-6"> Задаёт белый цвет тексу и нижней границе для поля ввода. <br>
          <code>border-bottom: 2px solid #f4f4f4; color: #f4f4f4</code>
        </p>
      </div>
    </li>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">gold</b>
        <p class="uk-width-5-6"> Задаёт жёлтый цвет тексу и нижней границе для поля ввода. <br>
          <code>border-bottom: 2px solid #ca9023; color: #ca9023</code>
        </p>
      </div>
    </li>
  </ul>
  <p>Компонент имеет модификатор <b>error</b>, который принимает значения <b>true</b> и <b>false</b>:</p>
  <ul>
    <li>
      <div class="uk-grid-small" uk-grid>
        <b class="uk-width-1-6">true / false</b>
        <p class="uk-width-5-6">
          Даёт возможность вручную указать валидность поля ввода.<br>
        </p>
      </div>
    </li>
  </ul>
  <br><br>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2">
      @include('components.input', ['label' => 'Name', 'style' => 'white', 'error' => false])
    </div>
    <div class="uk-width-1-2">
      @include('components.input', ['label' => 'Name', 'style' => 'white', 'error' => true])
    </div>
  </div>
  <br><br>
  <div class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2">
      @include('components.input', ['label' => 'Name', 'style' => 'gold', 'error' => false])
    </div>
    <div class="uk-width-1-2">
      @include('components.input', ['label' => 'Name', 'style' => 'gold', 'error' => true])
    </div>
  </div>
  <br><br>
	<?php $content = file_get_contents( base_path() . '/resources/views/examples/input.blade.php' ) ?>
  <pre class="uk-text-danger"><code>{{ $content }}</code></pre>

</div>
