<div class="uk-card uk-card-secondary uk-card-body">
  <h3 class="uk-card-title">c-about-short</h3>
  @include('components.about-short')
  <pre class="uk-text-danger uk-display-inline-block"><code>&#64;include('components.about-short')</code></pre>
</div>
