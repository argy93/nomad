@extends('layouts.cabinet')

@section('title', 'test')
@section('page', 'cabinet-test')
@section('content')
  @include('components.form-test', ['cabinet' => true, 'redirect' => route('cabinet.test-success')])
@endsection
