@extends('layouts.cabinet')

@section('title', 'home')
@section('page', 'cabinet-home')
@section('content')

  @include('components.personal-card', [
    'icon' => '/storage/common/test-icon.svg',
    'background' => '/storage/common/cards-map.svg',
    'title' => 'Pass the test to know which place is the best for your living',
    'href' => 'test',
    'link' => 'Pass the test',
    'disabled' => $test_passed
  ])

  @include('components.personal-card', [
    'icon' => '/storage/common/acheve-icon.svg',
    'background' => '/storage/common/BITS-glowing-logo.svg',
    'title' => 'Achieve flag 7 & get all nomad family advantages',
    'href' => 'achieve',
    'link' => 'Achieve flag 7',
  ])

  @include('components.personal-card', [
    'icon' => '/storage/common/compass-icon.svg',
    'background' => '/storage/common/card-logo.svg',
    'title' => 'CHOOSE THE CITY AND BUILD YOUR ONLINE BUSINESS WITH THIS TOOLS',
    'href' => 'tools',
    'link' => 'Get the tools',
  ])
@endsection
