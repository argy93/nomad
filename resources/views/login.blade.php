<?php if (!isset($redirect)) $redirect = route('cabinet.home') ?>

@extends('layouts.full-size')

@section('title', 'Login')
@section('page', 'login')
@section('content')
  @include('components.form-login', ['redirect' => $redirect])
@endsection
