<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nomad Home</title>
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
  <div class="p-home">
    <div class="l-header">
      @include('components.header')
    </div>
    <div class="l-navigation">
      @include('components.navigation', ['options' =>
        [
          [
            'anchor' => '/#hero',
            'text' => ''
          ],
          [
            'anchor' => '/#best-tools',
            'text' => '30 BEST TOOLS'
          ],
          [
            'anchor' => '/#flag-theory',
            'text' => 'FLAG THEORY'
          ],
          [
            'anchor' => '/#how-it-work',
            'text' => 'HOW IT WORKS'
          ],
          [
            'anchor' => '/#events',
            'text' => 'EVENTS'
          ],
          [
            'anchor' => '/#tests',
            'text' => 'TEST'
          ]
        ]
      ])
    </div>
    @include('components.hero', [
      'title' => 'Limitless. Secure. Wealthy.',
      'video' => '/storage/common/hero-video.mp4',
      'text' => 'Nomad Family is a worldwide network that helps thousands of people to transform their lives and achieve goals by traveling, living in any country they want, and building businesses as easy as ABC.'
    ])
    @include('components.about-short')
    <div class="uk-margin-xlarge-top">
      @include('components.citation')
    </div>
    <div class="uk-margin-large-top">
      @include('components.world-wide')
    </div>
    <div class="uk-margin-xlarge-top">
      @include('components.possibilities')
    </div>
    @include('components.fillup')
    <div class="uk-margin-xlarge-top" id="best-tools">
      @include('components.best-tools')
    </div>
    <div class="uk-margin-xlarge-top" id="flag-theory">
      @include('components.theory')
    </div>
    <div class="l-flags">
      <div class="u-container">
        @include('components.flags')
      </div>
    </div>
    <div class="l-how-works" id="how-it-work">
      <div class="u-container">
        @include('components.how-works')
      </div>
    </div>
    @include('components.profit')
    <div class="l-meeting" id="events">
      @include('components.meeting')
    </div>
    <div class="l-who">
      @include('components.who')
    </div>
    <div class="l-around">
      <div class="u-container">
        @include('components.around')
      </div>
    </div>
    <div class="l-upwork-stats">
      @include('components.upwork-stats')
    </div>
    <div class="l-club">
      @include('components.club')
    </div>
    <div class="l-criteria" id="tests">
      @include('components.criteria')
    </div>
    <div class="l-journey-box">
      @include('components.journey-box')
    </div>
    <div class="l-travel">
      @include('components.travel')
    </div>
    <div class="l-footer">
      <div class="u-container">
        @include('components.footer')
      </div>
    </div>
  </div>

  @include('components.mobile-menu')

<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
