<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="{{ mix('/css/ui.css') }}">
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
  <div class="p-ui uk-margin-top uk-margin-left uk-margin-right">

    <div class="uk-grid-column-small" uk-grid>
      <div class="uk-width-2-5">
        @include('examples.title-card')
      </div>
      <div class="uk-width-3-5">
        @include('examples.input-card')
        <div class="uk-margin-top">
          @include('examples.spinner-card')
        </div>
      </div>
      <div class="uk-width-2-5">
        @include('examples.text-card')
      </div>
      <div class="uk-width-3-5">
        @include('examples.error-card')
        <div class="uk-margin-top">
          @include('examples.link-card')
        </div>
      </div>
      <div class="uk-width-2-5">
        @include('examples.logo-card')
      </div>
      <div class="uk-width-3-5">
        @include('examples.checkbox')
      </div>
      <div class="uk-width-2-5">
        @include('examples.social-card')
        <div class="uk-margin-top">
          @include('examples.line-card')
        </div>
      </div>
      <div class="uk-width-3-5">
        @include('examples.button-card')
      </div>
      <div class="uk-width-2-5">
        @include('examples.quote')
      </div>
      <div class="uk-width-3-5">
        @include('examples.range-card')
      </div>
      <div class="uk-width-2-5">
        @include('examples.card')
      </div>
      <div class="uk-width-3-5">
        @include('examples.flag')
      </div>
      <div class="uk-width-2-5">
        @include('examples.navigation')
      </div>
      <div class="uk-width-3-5">
        @include('examples.spiral-gold')
      </div>
      <div class="uk-width-2-5">
        @include('examples.flag-progress')
      </div>
      <div class="uk-width-3-5">
        @include('examples.dropdown')
      </div>
      <div class="uk-width-2-5">
        @include('examples.ballons')
      </div>
      <div class="uk-width-3-5">
        @include('examples.form-criteria')
      </div>
      <div class="uk-width-2-5">
        @include('examples.members-counter')
      </div>
      <div class="uk-width-3-5">
        @include('examples.stopline-card')
      </div>
      <div class="uk-width-1-1">
        @include('examples.video')
      </div>
      <div class="uk-width-1-1">
        @include('examples.hs-slider')
      </div>
    </div>

    <div class="uk-grid-column-small" uk-grid>
      <div class="uk-width-1-1">
        @include('examples.header')
      </div>
      <div class="uk-width-1-1">
        @include('examples.hero')
      </div>
      <div class="uk-width-1-1">
        @include('examples.about-short')
      </div>
      <div class="uk-width-1-1">
        @include('examples.citation')
      </div>
      <div class="uk-width-1-1">
        @include('examples.map')
      </div>
      <div class="uk-width-1-1">
        @include('examples.possibilities')
      </div>
      <div class="uk-width-1-1">
        @include('examples.fillup')
      </div>
      <div class="uk-width-1-1">
        @include('examples.best-tools')
      </div>
      <div class="uk-width-1-1">
        @include('examples.theory')
      </div>
      <div class="uk-width-1-1">
        @include('examples.flags')
      </div>
      <div class="uk-width-1-1">
        @include('examples.how-works')
      </div>
      <div class="uk-width-1-1">
        @include('examples.profit')
      </div>
      <div class="uk-width-1-1">
        @include('examples.meeting')
      </div>
      <div class="uk-width-1-1">
        @include('examples.who')
      </div>
      <div class="uk-width-1-1">
        @include('examples.around')
      </div>
      <div class="uk-width-1-1">
        @include('examples.upwork-stats')
      </div>
      <div class="uk-width-1-1">
        @include('examples.club')
      </div>
      <div class="uk-width-1-1">
        @include('examples.criteria')
      </div>
      <div class="uk-width-1-1">
        @include('examples.journey-box')
      </div>
      <div class="uk-width-1-1">
        @include('examples.travel')
      </div>
      <div class="uk-width-1-1">
        @include('examples.footer')
      </div>
    </div>

    <div class="uk-grid-column-small" uk-grid>
      <div class="uk-width-1-1">
        @include('examples.test-intro')
      </div>
      <div class="uk-width-1-1">
        @include('examples.form')
      </div>
      <div class="uk-width-1-1">
        @include('examples.user')
      </div>
      <div class="uk-width-2-5">
        @include('examples.personal-card')
      </div>
      <div class="uk-width-3-5">
        @include('examples.card-advanced')
      </div>
      <div class="uk-width-1-1">
        @include('examples.features-centered')
      </div>
    </div>

  </div>

  <script src="{{ mix('/js/ui.js') }}"></script>
  <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
