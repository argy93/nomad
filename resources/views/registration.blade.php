<?php if (!isset($redirect)) $redirect = route('cabinet.home') ?>

@extends('layouts.full-size')

@section('title', 'Registration')
@section('page', 'registration')
@section('content')
  @include('components.form-register', ['redirect' => $redirect])
@endsection
