<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cabinet @yield('title') - Nomad</title>
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div class="p-@yield('page')">
  <div class="l-cabinet">
    <div class="l-cabinet__head">
      @include('components.header', ['cabinet' => true])
    </div>
    <div class="l-cabinet__body">
      <div class="l-cabinet__sidebar">@include('components.sidebar')</div>
      <div class="l-cabinet__content">
        <div class="l-cabinet__inner">
          @yield('content')
        </div>
      </div>
    </div>
    <div class="l-cabinet__footer">
      @include('components.cabinet-footer')
    </div>
  </div>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
