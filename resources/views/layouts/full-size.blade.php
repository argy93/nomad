<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title') - Nomad</title>
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div class="p-@yield('page')">
  <div class="l-full-size">
    <div class="l-full-size__bg">
      <img src="{{ asset('/storage/common/sea-w-auto@1x.jpg') }}" srcset="{{ srcset('/storage/common/sea@4x.jpg', 4) }}" alt="">
    </div>
    <div class="l-full-size__header">
      @include('components.header')
    </div>
    <div class="l-full-size__content">
      @yield('content')
    </div>
    <div class="l-full-size__travel">
      <div class="l-travel">
        @yield('travel')
      </div>
    </div>
    <div class="l-full-size__footer">
      <div class="l-footer">
        <div class="u-container">
          @yield('footer')
        </div>
      </div>
    </div>
  </div>
  @include('components.mobile-menu')
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
