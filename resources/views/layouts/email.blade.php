<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>
</head>
<body>
<div style="max-width: 600px; width:100%; background-color: #262626; color: #f4f4f4; padding: 15px 50px 15px 20px; border: 2px solid #CA9023; font-size: 14px; box-sizing: border-box;">
  <div style="text-align:center;">
    <img width="200px" src="{{asset('/storage/common/email-logo.png') }}" alt="Nomad Logo image">
  </div>
  <div style="margin-top: 50px;">
    @yield('content')
  </div>
</div>
</body>
</html>
