@extends('layouts.email')

@section('title', 'Nomad Test')
@section('page', 'login')
@section('content')
  Dear {{ $text["name"] }}. Thank you for join Nomad Family.<br>
  Nomad Family is a worldwide network that helps thousands of people to transform their lives and achieve goals by traveling, living in any country they want, and building businesses as easy as ABC.<br>
  There are more than 18000 nomads worldwide in our network and they all became successful people.<br>
  Some of our services are 8th Flag System, consulting and our partners product Bitsonar.com.<br>

  Firstly, you need to jump into our Dashboard and get free short book about the best  tools for travelers and business people.<br>
  Then, Pass Free test and get to know how much you could made online.<br>
  Secondly, you could use the best and famous worldwide tool Bitsonar to make your success from online activities.<br>
  <br>
  <p style="text-align:center;">
  <a href="{{ $text["link"] }}" style="
    cursor: pointer;
    display: inline-block;
    flex-shrink: 0;
    position: absolute;
    right: 0;
    bottom: 0;
    z-index: 1;
    box-sizing: border-box;
    max-width: 300px;
    width: 100%;
    height: 68px;
    padding: 0;
    border: none;
    line-height: 68px;
    text-align: center;
    text-transform: uppercase;
    font-size: 18px;
    font-weight: 700;
    overflow: hidden;
    color: #f4f4f4;
    background-color: #ca9023;
    margin-left: auto;
    margin-right: auto;
">Grab your opportunity</a>
  </p>

@endsection
