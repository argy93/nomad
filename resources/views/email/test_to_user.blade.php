@extends('layouts.email')

@section('title', 'Nomad Test')
@section('page', 'login')
@section('content')
  Dear {{ $text["name"] }}. Thank you to join Nomad Family and pass the Test.<br>
  There are more than 18000 nomads worldwide and they all became successful people.<br>
  Some of our services are 8th Flag System, consulting and our partners product Bitsonar.com. Below you will find your insights about the passing test.
  <br>
  <br>

  <ol>
    <li>
      <p><b>Countries to live</b></p>
      <p>{{ $text["p1"] }}</p>
    </li>

    <li>
      <p><b>Current income</b></p>
      <p>{{ $text["p2"] }}</p>
    </li>

    <li>
      <p><b>Desire income</b></p>
      <p>{{ $text["p3"] }}</p>
    </li>

    <li>
      <p><b>Available amount</b></p>
      <p>{{ $text["p4"] }}</p>
    </li>

    @if ($text["p5"] !== "")
      <li>
        <p><b>Spouse or not</b></p>
        <p>{{ $text["p5"] }}</p>
      </li>
    @endif

  </ol>
@endsection
